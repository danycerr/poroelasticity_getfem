//
//  problemPressure.cpp
//  
//
//  Created by Bianca Giovanardi on 22/06/16.
//
//

#include "gmm/gmm_inoutput.h"
#include "gmm/gmm_superlu_interface.h"
#include "problemMonolithic_nocrack.h"
#include "getfem/getfem_assembling.h"
#include "getfem/getfem_model_solvers.h"
#include "getfem/getfem_mesh_fem_level_set.h"

#define LAGRANGE 1

typedef getfem::model_real_plain_vector  plain_vector;

// Boundary data
class sigmaDataClass {
    std::vector<scalar_type> sigma_;
public:
    base_small_vector operator()(const base_node &x);
    void setSigma (const std::vector<scalar_type> sigma);
};

base_small_vector sigmaDataClass::operator()(const base_node &x) {
    int N = x.size();
    base_small_vector res(N);
    res[0] = sigma_[0];
    res[1] = sigma_[1];
    return res;
}

void sigmaDataClass::setSigma (const std::vector<scalar_type> sigma) {
    sigma_ = sigma;
}

base_small_vector dirichletData(const base_node &x) {
    int N = x.size();
    base_small_vector res(N);
    res[0] = 0;
    res[1] = 0;
    return res;
}

void problemMonolithic_nocrack::initialize(void) {

    nb_dof_pressure = (*mf_pressure_).nb_dof();
    nb_dof_displacement = (*mf_displacement_).nb_dof();
    
    //  The boundary regions:
    //      -  102  -
    //      |       |
    //     101     103
    //      |       |
    //      -  104   -
	//

    // Finding the Dirichlet dofs for displacement
    dirichletDofs1 = (*mf_pressure_).basic_dof_on_region(102);
    dirichletDofs2 = (*mf_pressure_).basic_dof_on_region(104);
    dirichletDofs3 = (*mf_pressure_).basic_dof_on_region(103);

}

void problemMonolithic_nocrack::initializeWithArtificialDirichletBC(const base_node artificialNode){
    
    this->initialize();
    
    // set boundary conditions
    std::cout << "Selecting Neumann and Dirichlet boundaries\n";
    getfem::mesh_region border_faces;
    getfem::outer_faces_of_mesh(*mesh_p_, border_faces);
    for (getfem::mr_visitor i(border_faces); !i.finished(); ++i) {
        for (scalar_type k= 0; k < 2; ++k) {
            base_node boundary_node = ((*mesh_p_).points_of_face_of_convex(i.cv(), i.f()))[k];
            if (gmm::abs(boundary_node[0] - artificialNode[0])<1.e-7 && gmm::abs(boundary_node[1] - artificialNode[1])<1.e-2) {
                std::cout << "Adding artificial Dirichlet condition for well-posedness on node (" << boundary_node[0] << ", " << boundary_node[1] << ");" << std::endl;
                (*mesh_p_).region(100).add(i.cv(), i.f());
            }
        }
    }
}

bool problemMonolithic_nocrack::solveAnalytic(std::vector<scalar_type> &p_new,
                                      std::vector<scalar_type> & u_new,
                                      const std::vector<scalar_type> & pressureAnalyticPrev,
                                      const std::vector<scalar_type> & openingAnalyticPrev,
                                      const std::vector<scalar_type> & openingDerivativeAnalyticPrev,
                                      const std::vector<scalar_type> load_top,
                                      const std::string BC_TYPE_TOP,
                                      const std::vector<scalar_type> load_bottom,
                                      const std::string BC_TYPE_BOT,
                                      const std::string lag) {
    
    // Model description.
    getfem::model model;
    
    // Main unknown of the problem.
    model.add_fem_variable("p", *mf_pressure_);
    // Main unknown of the problem.
    model.add_fem_variable("u", mf_u());

    model.add_initialized_fem_data("pprev", *mf_pressure_, pressureAnalyticPrev);
    model.add_initialized_fem_data("w", *mf_opening_, openingAnalyticPrev);

    //-----------------------------------------------
    // Solid Equation
    //-----------------------------------------------
    
    // Linearized elasticity brick.
    model.add_initialized_fixed_size_data("lambda", plain_vector(1, lambda_));
    model.add_initialized_fixed_size_data("mu", plain_vector(1, mu_));
    getfem::add_isotropic_linearized_elasticity_brick
    (model, *im_, "u", "lambda", "mu");
    std::cout << "Elasticity brick" << std::endl;
    
    // Pore pressure brick.
    model.add_initialized_fixed_size_data("alpha", plain_vector(1, alpha_));
    getfem::add_linear_generic_assembly_brick(model, *im_, "-alpha.p.Trace(Grad_Test_u)", -1);
    std::cout << "Pore pressure brick" << std::endl;
    
    // Fracture pressure brick.
    // NOTE: (2) because the normal in this case is [0,1]
    getfem::add_linear_generic_assembly_brick(model, *im_ls_, "-p*((Xfem_plus(Test_u))(2) - (Xfem_minus(Test_u))(2))", -1);
    std::cout << "Fracture pressure brick" << std::endl;
    
    // Left boundary (symmetry conditions)
    model.add_initialized_fixed_size_data("gamma1", plain_vector(1, 1e12));
    getfem::add_linear_generic_assembly_brick(model, *im_, "gamma1*u(1)*Test_u(1)",
                                              101, true, true);
    getfem::add_linear_generic_assembly_brick(model, *im_ls_, "gamma1*u(1)*Test_u(1)",
                                              101);
    
    std::cout << "Symmetry BC brick" << std::endl;
    
    // Boundary conditions bottom.
    std::vector<scalar_type> botBoundary((*mf_rhs).nb_dof()*N);
    sigmaDataClass sigmaDataBottom;
    sigmaDataBottom.setSigma(load_bottom);
    getfem::interpolation_function(*mf_rhs, botBoundary, sigmaDataBottom);
    
    if (BC_TYPE_BOT == "Displacement") {
        model.add_initialized_fem_data("DirichletDataBot", *mf_rhs, botBoundary);
        getfem::add_Dirichlet_condition_with_penalization
        (model, *im_, "u", 1e18, 104, "DirichletDataBot");
    }
    else if (BC_TYPE_BOT == "Stress") {
        model.add_initialized_fem_data("NeumannBoundaryDataBot", *mf_rhs, botBoundary);
        getfem::add_source_term_brick(model, *im_, "u", "NeumannBoundaryDataBot", 104);
    }
    
    // Boundary conditions top.
    std::vector<scalar_type> topBoundary((*mf_rhs).nb_dof()*N);
    sigmaDataClass sigmaDataTop;
    sigmaDataTop.setSigma(load_top);
    getfem::interpolation_function(*mf_rhs, topBoundary, sigmaDataTop);
    
    if (BC_TYPE_TOP == "Displacement") {
        model.add_initialized_fem_data("DirichletDataTop", *mf_rhs, topBoundary);
        getfem::add_Dirichlet_condition_with_penalization
        (model, *im_, "u", 1e18, 102, "DirichletDataTop");
    }
    else if (BC_TYPE_TOP == "Stress") {
        model.add_initialized_fem_data("NeumannBoundaryDataTop", *mf_rhs, topBoundary);
        getfem::add_source_term_brick(model, *im_, "u", "NeumannBoundaryDataTop", 102);
    }
    
    std::vector<scalar_type> artficialData((*mf_rhs).nb_dof()*N);
    getfem::interpolation_function(*mf_rhs, artficialData, dirichletData);
    model.add_initialized_fem_data("ArtificialDirichletData", *mf_rhs, artficialData);
    getfem::add_Dirichlet_condition_with_penalization
    (model, *im_, "u", 1e18, 100, "ArtificialDirichletData");
    
    std::cout << "Displacement BC brick" << std::endl;
    
    //-----------------------------------------------
    // Fluid Equation
    //-----------------------------------------------
    
    // grad(p).grad(test)
    model.add_initialized_fixed_size_data("permeabilityOverViscosityX", plain_vector(1, permeabilityX_/viscosity_));
    getfem::add_linear_generic_assembly_brick(model, *im_, "permeabilityOverViscosityX*Grad_p(1).Grad_Test_p(1)",
                                              -1, true, true);
    
    model.add_initialized_fixed_size_data("permeabilityOverViscosityY", plain_vector(1, permeabilityY_/viscosity_));
    getfem::add_linear_generic_assembly_brick(model, *im_, "permeabilityOverViscosityY*Grad_p(2).Grad_Test_p(2)",
                                              -1, true, true);
    
    std::cout << "Assembled grad-grad on Omega" << std::endl;
    
    // w^3/12mu
    
    // Use the Newton linearization for w^3
    model.add_initialized_fixed_size_data("oneTwelfthMu", plain_vector(1, 1./(12*viscosity_)));
    getfem::add_linear_generic_assembly_brick(model, *im_ls_, "oneTwelfthMu*w*w*w*Grad_p(1)*(0.5*Xfem_plus(Grad_Test_p)(1)+0.5*Xfem_minus(Grad_Test_p)(1))",
                                              -1);
    
    model.add_initialized_fixed_size_data("oneFourthMu", plain_vector(1, 1./(4*viscosity_)));
    getfem::add_linear_generic_assembly_brick(model, *im_ls_, "oneFourthMu*w*w*w*Grad_pprev(1)*(0.5*Xfem_plus(Grad_Test_p)(1)+0.5*Xfem_minus(Grad_Test_p)(1))",
                                              -1);
    
    getfem::add_source_term_generic_assembly_brick(model, *im_ls_, "oneFourthMu*w*w*w*Grad_pprev(1)*(0.5*Xfem_plus(Grad_Test_p)(1)+0.5*Xfem_minus(Grad_Test_p)(1))", -1);
    
    // Extra term (permeability along x in the crack)
    getfem::add_linear_generic_assembly_brick(model, *im_ls_, "w*permeabilityOverViscosityX*(0.5*(Xfem_plus(Grad_p))(1)+0.5*(Xfem_minus(Grad_p))(1)).(0.5*Xfem_plus(Grad_Test_p)(1)+0.5*Xfem_minus(Grad_Test_p)(1))",
                                              -1, true, true);
    
    getfem::add_linear_generic_assembly_brick(model, *im_ls_, "w*permeabilityOverViscosityX*(0.5*(Xfem_plus(Grad_pprev))(1)+0.5*(Xfem_minus(Grad_pprev))(1)).(0.5*Xfem_plus(Grad_Test_p)(1)+0.5*Xfem_minus(Grad_Test_p)(1))",
                                              -1);
    
    getfem::add_source_term_generic_assembly_brick(model, *im_ls_, "w*permeabilityOverViscosityX*(0.5*(Xfem_plus(Grad_pprev))(1)+0.5*(Xfem_minus(Grad_pprev))(1)).(0.5*Xfem_plus(Grad_Test_p)(1)+0.5*Xfem_minus(Grad_Test_p)(1))", -1);
    
    std::cout << "Assembled grad-grad on Gamma" << std::endl;
    
#if LAGRANGE 
    dal::bit_vector allConvexes = (*mesh_p_).convex_index();
    dal::bit_vector cutConvexes;
    bgeot::size_type i;
    for (i << allConvexes; i != bgeot::size_type(-1); i << allConvexes) {
        if ((*mls_).is_convex_cut(i)) {
            auto pressureDofs = (*mf_pressure_).ind_basic_dof_of_element(i);
            if (pressureDofs.size()==6) {
                cutConvexes.add(i);
                (*mesh_p_).region(43).add(i);
            }
        }
    }
    
    getfem::pfem pf_mult = getfem::fem_descriptor("FEM_PK(2, 0)");
    getfem::mesh_fem mf_mult(*mesh_p_);
    mf_mult.set_finite_element(cutConvexes, pf_mult);
    size_type mult_dofs = mf_mult.nb_dof();
    
    model.add_fem_variable("multiplier", mf_mult);
    getfem::add_linear_generic_assembly_brick(model, *im_ls_, "-permeabilityOverViscosityY*Test_multiplier*(Xfem_plus(p)-Xfem_minus(p))", 43);
    getfem::add_linear_generic_assembly_brick(model, *im_ls_, "-permeabilityOverViscosityY*multiplier*(Xfem_plus(Test_p)-Xfem_minus(Test_p))", 43);

#else
    // Penalizing jump
    std::cout << "Penalizing jump with gamma = .1" << std::endl;
    model.add_initialized_fixed_size_data("Gamma", plain_vector(1, .1));
    getfem::add_linear_generic_assembly_brick(model, *im_ls_, "Gamma*(Xfem_plus(p)-Xfem_minus(p))*(Xfem_plus(Test_p)-Xfem_minus(Test_p))", -1, true, true);
#endif
    std::cout << "Set continuity of pressure" << std::endl;
    
    getfem::add_linear_generic_assembly_brick(model, *im_ls_, "permeabilityOverViscosityY*(0.5*Xfem_plus(Grad_p)(2) + 0.5*Xfem_minus(Grad_p)(2))*(Xfem_plus(Test_p)-Xfem_minus(Test_p))", -1);
    
    getfem::add_linear_generic_assembly_brick(model, *im_ls_, "permeabilityOverViscosityY*(0.5*Xfem_plus(Grad_Test_p)(2) + 0.5*Xfem_minus(Grad_Test_p)(2))*(Xfem_plus(p)-Xfem_minus(p))", -1);

    // Opening derivative
    model.add_initialized_fem_data("dwdt", *mf_opening_, openingDerivativeAnalyticPrev);
    getfem::add_source_term_generic_assembly_brick(model, *im_ls_, "dwdt*(0.5*(Xfem_plus(Test_p))+0.5*(Xfem_minus(Test_p)))", -1, "Opening derivative");
    std::cout << "Assembled opening derivative" << std::endl;

    // Dirichlet boundary conditions
    model.add_initialized_fixed_size_data("p0", plain_vector(1, 0));
    add_Dirichlet_condition_with_simplification(model, "p", 102, "p0");
    add_Dirichlet_condition_with_simplification(model, "p", 103, "p0");
    add_Dirichlet_condition_with_simplification(model, "p", 104, "p0");
   
    // Lag
    if (lag != "") {
        model.add_initialized_fixed_size_data("gam", plain_vector(1, 10.));
        std::string penalizationLag = "gam*Heaviside(X(1)-" + lag + ")*(0.5*(Xfem_plus(p))+0.5*(Xfem_minus(p)))*(0.5*(Xfem_plus(Test_p))+0.5*(Xfem_minus(Test_p)))";
        getfem::add_linear_generic_assembly_brick(model, *im_ls_, penalizationLag,
                                              -1, true, true);
    }
    
    std::cout << "Set Dirichlet boundary conditions" << std::endl;
    
    //-----------------------------------------------
    // Assembly
    //-----------------------------------------------
    
    double time;
#ifdef GMM_USES_MPI
    time = MPI_Wtime();
#else
    time = gmm::uclock_sec();
#endif
    
    std::cout << "Assemblying the linear system..." << std::endl;
    
    model.assembly(getfem::model::BUILD_ALL);
    auto A = model.real_tangent_matrix();
    auto F = model.real_rhs();
    
#if LAGRANGE
    // Setting Inflow
    for(size_type i = 0; i < (*mf_pressure_).nb_dof(); ++i) {
        scalar_type x = (*mf_pressure_).point_of_basic_dof(i)[0];
        scalar_type y = (*mf_pressure_).point_of_basic_dof(i)[1];
        if (x==0 && y==0) {
            std::cout << "Setting inflow at node (0,0)" << std::endl;
            F[i+mult_dofs] += 0.5*inflow_;
        }
    }
#else
    // Setting Inflow
    for(size_type i = 0; i < (*mf_pressure_).nb_dof(); ++i) {
        scalar_type x = (*mf_pressure_).point_of_basic_dof(i)[0];
        scalar_type y = (*mf_pressure_).point_of_basic_dof(i)[1];
        if (x==0 && y==0) {
            std::cout << "Setting inflow at node (0,0)" << std::endl;
            F[i] += 0.5*inflow_;
        }
    }
#endif
    
    std::cout << "Solving the linear system..." << std::endl;
    
    std::vector<scalar_type> U;
#if LAGRANGE
    U.resize(nb_dof_pressure+nb_dof_displacement+mult_dofs);
#else
    U.resize(nb_dof_pressure+nb_dof_displacement);
#endif
    
    scalar_type rcond;
    int done=SuperLU_solve(A, U, F, rcond);
    
    double ftime;
#ifdef GMM_USES_MPI
    ftime = MPI_Wtime();
#else
    ftime = gmm::uclock_sec();
#endif
    
    
#if LAGRANGE
    gmm::copy(gmm::sub_vector(U, gmm::sub_interval(mult_dofs,nb_dof_pressure)),
              p_new);
    
    gmm::copy(gmm::sub_vector(U, gmm::sub_interval(mult_dofs+nb_dof_pressure,nb_dof_displacement)),
              u_new);
#else
    gmm::copy(gmm::sub_vector(U, gmm::sub_interval(0,nb_dof_pressure)),
              p_new);
    
    gmm::copy(gmm::sub_vector(U, gmm::sub_interval(nb_dof_pressure,nb_dof_displacement)),
              u_new);
#endif
    std::cout << "Solved problem in " << ftime - time << " seconds." << std::endl;
    
    return 1;
    
}

bool problemMonolithic_nocrack::solve(std::vector<scalar_type> &p_new, std::vector<scalar_type> & u_new, const std::vector<scalar_type> & pressurePrev, const std::vector<scalar_type> & displacementPrev, const std::vector<scalar_type> & pressureOld, const std::vector<scalar_type> & displacementOld, const bool Static, const std::vector<scalar_type> &source, const std::string DISPLACEMENT_TYPE, const std::vector<scalar_type> load_top, const std::string BC_TYPE_TOP, const std::vector<scalar_type> load_bottom, const std::string BC_TYPE_BOT, const std::string lag) {
    
    // Model description.
    getfem::model model;
    
    // Main unknown of the problem.
    model.add_fem_variable("p", *mf_pressure_);
    // Main unknown of the problem.
    model.add_fem_variable("u", mf_u());
    
    //-----------------------------------------------
    // Solid Equation
    //-----------------------------------------------
    
    // Linearized elasticity brick.
    model.add_initialized_fixed_size_data("lambda", plain_vector(1, lambda_));
    model.add_initialized_fixed_size_data("mu", plain_vector(1, mu_));
    getfem::add_isotropic_linearized_elasticity_brick
    (model, *im_, "u", "lambda", "mu");
    std::cout << "Elasticity brick" << std::endl;
    
    // Pore pressure brick.
    model.add_initialized_fixed_size_data("alpha", plain_vector(1, alpha_));
    getfem::add_linear_generic_assembly_brick(model, *im_, "-alpha.p.Trace(Grad_Test_u)", -1);
    std::cout << "Pore pressure brick" << std::endl;
    
    // Fracture pressure brick.
    // NOTE: (2) because the normal in this case is [0,1]
    getfem::add_linear_generic_assembly_brick(model, *im_ls_, "-p*((Xfem_plus(Test_u))(2) - (Xfem_minus(Test_u))(2))", -1);
    std::cout << "Fracture pressure brick" << std::endl;
    
    // Left boundary (symmetry conditions)
    model.add_initialized_fixed_size_data("gamma1", plain_vector(1, 1e12));
    getfem::add_linear_generic_assembly_brick(model, *im_, "gamma1*u(1)*Test_u(1)",
                                              101, true, true);
    getfem::add_linear_generic_assembly_brick(model, *im_ls_, "gamma1*u(1)*Test_u(1)",
                                              101);
    
    std::cout << "Symmetry BC brick" << std::endl;
    
    // Boundary conditions bottom.
    std::vector<scalar_type> botBoundary((*mf_rhs).nb_dof()*N);
    sigmaDataClass sigmaDataBottom;
    sigmaDataBottom.setSigma(load_bottom);
    getfem::interpolation_function(*mf_rhs, botBoundary, sigmaDataBottom);
    
    if (BC_TYPE_BOT == "Displacement") {
        model.add_initialized_fem_data("DirichletDataBot", *mf_rhs, botBoundary);
        getfem::add_Dirichlet_condition_with_penalization
        (model, *im_, "u", 1e18, 104, "DirichletDataBot");
    }
    else if (BC_TYPE_BOT == "Stress") {
        model.add_initialized_fem_data("NeumannBoundaryDataBot", *mf_rhs, botBoundary);
        getfem::add_source_term_brick(model, *im_, "u", "NeumannBoundaryDataBot", 104);
    }
    
    // Boundary conditions top.
    std::vector<scalar_type> topBoundary((*mf_rhs).nb_dof()*N);
    sigmaDataClass sigmaDataTop;
    sigmaDataTop.setSigma(load_top);
    getfem::interpolation_function(*mf_rhs, topBoundary, sigmaDataTop);
    
    if (BC_TYPE_TOP == "Displacement") {
        model.add_initialized_fem_data("DirichletDataTop", *mf_rhs, topBoundary);
        getfem::add_Dirichlet_condition_with_penalization
        (model, *im_, "u", 1e18, 102, "DirichletDataTop");
    }
    else if (BC_TYPE_TOP == "Stress") {
        model.add_initialized_fem_data("NeumannBoundaryDataTop", *mf_rhs, topBoundary);
        getfem::add_source_term_brick(model, *im_, "u", "NeumannBoundaryDataTop", 102);
    }
    
    std::vector<scalar_type> artficialData((*mf_rhs).nb_dof()*N);
    getfem::interpolation_function(*mf_rhs, artficialData, dirichletData);
    model.add_initialized_fem_data("ArtificialDirichletData", *mf_rhs, artficialData);
    getfem::add_Dirichlet_condition_with_penalization
    (model, *im_, "u", 1e18, 100, "ArtificialDirichletData");
    
    std::cout << "Displacement BC brick" << std::endl;

    //-----------------------------------------------
    // Fluid Equation
    //-----------------------------------------------
    
    // grad(p).grad(test)
    model.add_initialized_fixed_size_data("permeabilityOverViscosityX", plain_vector(1, permeabilityX_/viscosity_));
    getfem::add_linear_generic_assembly_brick(model, *im_, "permeabilityOverViscosityX*Grad_p(1).Grad_Test_p(1)",
                                              -1, true, true);
    
    model.add_initialized_fixed_size_data("permeabilityOverViscosityY", plain_vector(1, permeabilityY_/viscosity_));
    getfem::add_linear_generic_assembly_brick(model, *im_, "permeabilityOverViscosityY*Grad_p(2).Grad_Test_p(2)",
                                              -1, true, true);
    
    std::cout << "Assembled grad-grad on Omega" << std::endl;
    
    // w^3/12mu
    if(DISPLACEMENT_TYPE == "NUMERIC") {
        
        /* To check sign of opening
        getfem::model model2;
        // Main unknown of the problem.
        model2.add_fem_variable("u", *mf_displacement_);
        getfem::add_source_term_generic_assembly_brick(model2, *im_ls_, "((Xfem_plus(Test_u))(2)-(Xfem_minus(Test_u))(2))", -1);
        std::vector<scalar_type> openingInt;
        model2.assembly(getfem::model::BUILD_RHS);
        auto F = model2.real_rhs();
        
        for(size_type i = 0; i < (*mf_displacement_).nb_dof(); ++i) {
            if (F[i]){
                scalar_type x = (*mf_displacement_).point_of_basic_dof(i)[0];
                scalar_type y = (*mf_displacement_).point_of_basic_dof(i)[1];
                std::cout << "(x,y) = (" << x << ", " << y << ") \t" << F[i] << std::endl;
            }
        }*/
        
        // Use a given opening for w^3
        //model.add_initialized_fixed_size_data("w3", plain_vector(1, 1.e-9));
        //model.add_initialized_fixed_size_data("oneTwelfthMu", plain_vector(1, 1./(12*viscosity_)));
        //getfem::add_linear_generic_assembly_brick(model, *im_ls_, "oneTwelfthMu*w3*Grad_p(1)*Grad_Test_p(1)", -1, true, true);

        // Use the previous opening for w^3
        //model.add_initialized_fem_data("uprev", *mf_displacement_, displacementPrev);
        //model.add_initialized_fixed_size_data("oneTwelfthMu", plain_vector(1, 1./(12*viscosity_)));
        //getfem::add_linear_generic_assembly_brick(model, *im_ls_, "oneTwelfthMu*((Xfem_plus(uprev))(2)-(Xfem_minus(uprev))(2))*((Xfem_plus(uprev))(2)-(Xfem_minus(uprev))(2))*((Xfem_plus(uprev))(2)-(Xfem_minus(uprev))(2))*Grad_p(1)*Grad_Test_p(1)", -1, true, true);
        
        // Use the Newton linearization for w^3
        model.add_initialized_fem_data("uprev", *mf_displacement_, displacementPrev);
        model.add_initialized_fem_data("pprev", *mf_pressure_, pressurePrev);
        model.add_initialized_fixed_size_data("oneTwelfthMu", plain_vector(1, 1./(12*viscosity_)));
        getfem::add_linear_generic_assembly_brick(model, *im_ls_, "oneTwelfthMu*((Xfem_plus(uprev))(2)-(Xfem_minus(uprev))(2))*((Xfem_plus(uprev))(2)-(Xfem_minus(uprev))(2))*((Xfem_plus(uprev))(2)-(Xfem_minus(uprev))(2))*Grad_p(1)*(0.5*Xfem_plus(Grad_Test_p)(1)+0.5*Xfem_minus(Grad_Test_p)(1))",
                                                  -1);
        
        model.add_initialized_fixed_size_data("oneFourthMu", plain_vector(1, 1./(4*viscosity_)));
        getfem::add_linear_generic_assembly_brick(model, *im_ls_, "oneFourthMu*((Xfem_plus(uprev))(2)-(Xfem_minus(uprev))(2))*((Xfem_plus(uprev))(2)-(Xfem_minus(uprev))(2))*((Xfem_plus(u))(2)-(Xfem_minus(u))(2))*Grad_pprev(1)*(0.5*Xfem_plus(Grad_Test_p)(1)+0.5*Xfem_minus(Grad_Test_p)(1))",
                                                  -1);
        
        getfem::add_source_term_generic_assembly_brick(model, *im_ls_, "oneFourthMu*((Xfem_plus(uprev))(2)-(Xfem_minus(uprev))(2))*((Xfem_plus(uprev))(2)-(Xfem_minus(uprev))(2))*((Xfem_plus(uprev))(2)-(Xfem_minus(uprev))(2))*Grad_pprev(1)*(0.5*Xfem_plus(Grad_Test_p)(1)+0.5*Xfem_minus(Grad_Test_p)(1))", -1);
        
        // Extra term (permeability along x in the crack)
        getfem::add_linear_generic_assembly_brick(model, *im_ls_, "((Xfem_plus(uprev))(2)-(Xfem_minus(uprev))(2))*permeabilityOverViscosityX*(0.5*(Xfem_plus(Grad_p))(1)+0.5*(Xfem_minus(Grad_p))(1)).(0.5*Xfem_plus(Grad_Test_p)(1)+0.5*Xfem_minus(Grad_Test_p)(1))",
                                                  -1, true, true);

        getfem::add_linear_generic_assembly_brick(model, *im_ls_, "((Xfem_plus(u))(2)-(Xfem_minus(u))(2))*permeabilityOverViscosityX*(0.5*(Xfem_plus(Grad_pprev))(1)+0.5*(Xfem_minus(Grad_pprev))(1)).(0.5*Xfem_plus(Grad_Test_p)(1)+0.5*Xfem_minus(Grad_Test_p)(1))",
                                                  -1);
        
        getfem::add_source_term_generic_assembly_brick(model, *im_ls_, "((Xfem_plus(uprev))(2)-(Xfem_minus(uprev))(2))*permeabilityOverViscosityX*(0.5*(Xfem_plus(Grad_pprev))(1)+0.5*(Xfem_minus(Grad_pprev))(1)).(0.5*Xfem_plus(Grad_Test_p)(1)+0.5*Xfem_minus(Grad_Test_p)(1))", -1);

        if (!Static) {
            
            /*
            scalar_type aa = getfem::asm_H1_norm (*im_, *mf_displacement_, displacementOld);
            scalar_type bb = getfem::asm_H1_norm (*im_, *mf_pressure_, pressureOld);

            scalar_type cc = getfem::asm_H1_norm (*im_, *mf_displacement_, displacementPrev);
            scalar_type dd = getfem::asm_H1_norm (*im_, *mf_pressure_, pressurePrev);

            std::cout << "Norm uold = " << aa << "\t Norm pold = " << bb << std::endl;
            std::cout << "Norm uprev = " << cc << "\t Norm pprev = " << dd << std::endl;
            */
            
            model.add_initialized_fixed_size_data("deltaT", plain_vector(1, delta_t_));
            model.add_initialized_fem_data("pold", *mf_pressure_, pressureOld);
            model.add_initialized_fem_data("uold", *mf_displacement_, displacementOld);
            scalar_type s0 = (1.e-6)/68.7;
            model.add_initialized_fixed_size_data("s0", plain_vector(1, s0));
            
            getfem::add_source_term_generic_assembly_brick(model, *im_, "s0*pold*Test_p/deltaT", -1, "Pressure derivative");
            getfem::add_linear_generic_assembly_brick(model, *im_, "s0*p*Test_p/deltaT", -1, 1, 1);
            std::cout << "Assembled pressure derivative" << std::endl;
            
            getfem::add_source_term_generic_assembly_brick(model, *im_, "alpha*Trace(Grad_uold)*Test_p/deltaT", -1, "Divergence derivative");
            getfem::add_linear_generic_assembly_brick(model, *im_, "alpha*Trace(Grad_u)*Test_p/deltaT", -1);
            std::cout << "Assembled displacement divergence derivative" << std::endl;
            
            getfem::add_source_term_generic_assembly_brick(model, *im_ls_, "((Xfem_plus(uold))(2)-(Xfem_minus(uold))(2))*(0.5*Xfem_plus(Test_p)+0.5*Xfem_minus(Test_p))/deltaT", -1, "Opening derivative");

            getfem::add_linear_generic_assembly_brick(model, *im_ls_, "((Xfem_plus(u))(2)-(Xfem_minus(u))(2))*(0.5*Xfem_plus(Test_p)+0.5*Xfem_minus(Test_p))/deltaT", -1);

            std::cout << "Assembled opening derivative" << std::endl;
        }
        else {
            // Opening derivative
            model.add_initialized_fem_data("dwdt", *mf_opening_, source);
            getfem::add_source_term_generic_assembly_brick(model, *im_ls_, "dwdt*(0.5*(Xfem_plus(Test_p))+0.5*(Xfem_minus(Test_p)))", -1, "Opening derivative");
            std::cout << "Assembled opening derivative" << std::endl;
        }
    }
    else if (DISPLACEMENT_TYPE == "GUESS") {
        model.add_initialized_fem_data("w", *mf_opening_, displacementPrev);
        model.add_initialized_fem_data("pprev", *mf_pressure_, pressurePrev);
        model.add_initialized_fixed_size_data("twelveViscosity", plain_vector(1, 12*viscosity_));
        
        getfem::add_linear_generic_assembly_brick(model, *im_ls_, "(w*w*w/twelveViscosity)*Grad_p(1).(0.5*Xfem_plus(Grad_Test_p)(1)+0.5*Xfem_minus(Grad_Test_p)(1))",
                                                  -1);
        
        model.add_initialized_fixed_size_data("fourViscosity", plain_vector(1, 4*viscosity_));
        getfem::add_linear_generic_assembly_brick(model, *im_ls_, "(w*w/fourViscosity)*((Xfem_plus(u))(2)-(Xfem_minus(u))(2))*Grad_pprev(1).(0.5*Xfem_plus(Grad_Test_p)(1)+0.5*Xfem_minus(Grad_Test_p)(1))",
                                                  -1);
        
        getfem::add_source_term_generic_assembly_brick(model, *im_ls_, "(w*w*w/fourViscosity)*Grad_pprev(1).(0.5*Xfem_plus(Grad_Test_p)(1)+0.5*Xfem_minus(Grad_Test_p)(1))", -1);
        
         // Extra term (permeability along x in the crack)
         getfem::add_linear_generic_assembly_brick(model, *im_ls_, "w*permeabilityOverViscosityX*(0.5*Xfem_plus(Grad_p)(1)+0.5*Xfem_minus(Grad_p)(1)).(0.5*Xfem_plus(Grad_Test_p)(1)+0.5*Xfem_minus(Grad_Test_p)(1))",
         -1, true, true);

         getfem::add_linear_generic_assembly_brick(model, *im_ls_, "((Xfem_plus(u))(2)-(Xfem_minus(u))(2))*permeabilityOverViscosityX*(0.5*Xfem_plus(Grad_pprev)(1)+0.5*Xfem_minus(Grad_pprev)(1)).(0.5*Xfem_plus(Grad_Test_p)(1)+0.5*Xfem_minus(Grad_Test_p)(1))",
         -1);

         getfem::add_source_term_generic_assembly_brick(model, *im_ls_, "w*permeabilityOverViscosityX*(0.5*Xfem_plus(Grad_pprev)(1)+0.5*Xfem_minus(Grad_pprev)(1)).(0.5*Xfem_plus(Grad_Test_p)(1)+0.5*Xfem_minus(Grad_Test_p)(1))", -1);

    }
    std::cout << "Assembled grad-grad on Gamma" << std::endl;
    
#if LAGRANGE
    dal::bit_vector allConvexes = (*mesh_p_).convex_index();
    dal::bit_vector cutConvexes;
    bgeot::size_type i;
    for (i << allConvexes; i != bgeot::size_type(-1); i << allConvexes) {
        if ((*mls_).is_convex_cut(i)) {
            auto pressureDofs = (*mf_pressure_).ind_basic_dof_of_element(i);
            if (pressureDofs.size()==6) {
                cutConvexes.add(i);
                (*mesh_p_).region(43).add(i);
            }
        }
    }
    
    getfem::pfem pf_mult = getfem::fem_descriptor("FEM_PK(2, 0)");
    getfem::mesh_fem mf_mult(*mesh_p_);
    mf_mult.set_finite_element(cutConvexes, pf_mult);
    size_type mult_dofs = mf_mult.nb_dof();
    
    model.add_fem_variable("multiplier", mf_mult);
    getfem::add_linear_generic_assembly_brick(model, *im_ls_, "-permeabilityOverViscosityY*Test_multiplier*(Xfem_plus(p)-Xfem_minus(p))", 43);
    getfem::add_linear_generic_assembly_brick(model, *im_ls_, "-permeabilityOverViscosityY*multiplier*(Xfem_plus(Test_p)-Xfem_minus(Test_p))", 43);
#else
    /*
    model.add_initialized_fixed_size_data("Gamma", plain_vector(1, .0002));
    getfem::add_linear_generic_assembly_brick(model, *im_ls_, "Gamma*(Xfem_plus(p)-Xfem_minus(p))*(Xfem_plus(Test_p)-Xfem_minus(Test_p))/element_size", -1, true, true);*/

    //if(Static) {
        std::cout << "Penalizing jump with gamma = .1" << std::endl;
        model.add_initialized_fixed_size_data("Gamma", plain_vector(1, .1));
        getfem::add_linear_generic_assembly_brick(model, *im_ls_, "Gamma*(Xfem_plus(p)-Xfem_minus(p))*(Xfem_plus(Test_p)-Xfem_minus(Test_p))", -1, true, true);

    /*} else {
        std::cout << "Penalizing jump with gamma = 10" << std::endl;
        model.add_initialized_fixed_size_data("Gamma", plain_vector(1, 1.e6));
        getfem::add_linear_generic_assembly_brick(model, *im_ls_, "Gamma*(Xfem_plus(p)-Xfem_minus(p))*(Xfem_plus(Test_p)-Xfem_minus(Test_p))", -1, true, true);
    }*/
#endif
    std::cout << "Set continuity of pressure" << std::endl;
    
    getfem::add_linear_generic_assembly_brick(model, *im_ls_, "permeabilityOverViscosityY*(0.5*Xfem_plus(Grad_p)(2) + 0.5*Xfem_minus(Grad_p)(2))*(Xfem_plus(Test_p)-Xfem_minus(Test_p))", -1);
    
    getfem::add_linear_generic_assembly_brick(model, *im_ls_, "permeabilityOverViscosityY*(0.5*Xfem_plus(Grad_Test_p)(2) + 0.5*Xfem_minus(Grad_Test_p)(2))*(Xfem_plus(p)-Xfem_minus(p))", -1);

    // Dirichlet boundary conditions
    model.add_initialized_fixed_size_data("p0", plain_vector(1, 0));
    add_Dirichlet_condition_with_simplification(model, "p", 102, "p0");
    add_Dirichlet_condition_with_simplification(model, "p", 103, "p0");
    add_Dirichlet_condition_with_simplification(model, "p", 104, "p0");
    
    // Lag
    if (lag != "") {
        model.add_initialized_fixed_size_data("gam", plain_vector(1, 10.));
        std::string penalizationLag = "gam*Heaviside(X(1)-" + lag + ")*(0.5*(Xfem_plus(p))+0.5*(Xfem_minus(p)))*(0.5*(Xfem_plus(Test_p))+0.5*(Xfem_minus(Test_p)))";
        getfem::add_linear_generic_assembly_brick(model, *im_ls_, penalizationLag,
                                                  -1, true, true);
    }
    
    std::cout << "Set Dirichlet boundary conditions" << std::endl;

    //-----------------------------------------------
    // Assembly
    //-----------------------------------------------

    double time;
#ifdef GMM_USES_MPI
    time = MPI_Wtime();
#else
    time = gmm::uclock_sec();
#endif

    std::cout << "Assemblying the linear system..." << std::endl;

    model.assembly(getfem::model::BUILD_ALL);
    auto A = model.real_tangent_matrix();
    auto F = model.real_rhs();

#if LAGRANGE
    for(size_type i = 0; i < (*mf_pressure_).nb_dof(); ++i) {
        scalar_type x = (*mf_pressure_).point_of_basic_dof(i)[0];
        scalar_type y = (*mf_pressure_).point_of_basic_dof(i)[1];
        if (x==0 && y==0) {
            std::cout << "Setting inflow at node (0,0)" << std::endl;
            F[i+mult_dofs] += 0.5*inflow_;
        }
    }
#else
    // Setting Inflow
    for(size_type i = 0; i < (*mf_pressure_).nb_dof(); ++i) {
        scalar_type x = (*mf_pressure_).point_of_basic_dof(i)[0];
        scalar_type y = (*mf_pressure_).point_of_basic_dof(i)[1];
        if (x==0 && y==0) {
            std::cout << "Setting inflow at node (0,0)" << std::endl;
            F[i] += 0.5*inflow_;
        }
    }
    /*
    // Setting inlet pressure
    scalar_type ggamma = 1.e6;
    for(size_type i = 0; i < (*mf_pressure_).nb_dof(); ++i) {
        scalar_type x = (*mf_pressure_).point_of_basic_dof(i)[0];
        scalar_type y = (*mf_pressure_).point_of_basic_dof(i)[1];
        if (x==0 && y==0) {
            A[i][i] += ggamma;
            F[i] += ggamma*inflow_;
        }
    }*/
#endif
    /*
    std::ofstream ff("rhs.mm");
    for(size_type i = 0; i < nb_dof_pressure+nb_dof_displacement; ++i) {
        ff << F[i] << std::endl;
    }
    gmm::MatrixMarket_IO::write("A.mm" , A);
    */


    std::cout << "Solving the linear system..." << std::endl;
    
    std::vector<scalar_type> U;
#if LAGRANGE
    U.resize(nb_dof_pressure+nb_dof_displacement+mult_dofs);
#else
    U.resize(nb_dof_pressure+nb_dof_displacement);
#endif
    
    scalar_type rcond;
    int done=SuperLU_solve(A, U, F, rcond);
    
    double ftime;
#ifdef GMM_USES_MPI
    ftime = MPI_Wtime();
#else
    ftime = gmm::uclock_sec();
#endif
    
#if LAGRANGE
    gmm::copy(gmm::sub_vector(U, gmm::sub_interval(mult_dofs,nb_dof_pressure)),
              p_new);
    
    gmm::copy(gmm::sub_vector(U, gmm::sub_interval(mult_dofs+nb_dof_pressure,nb_dof_displacement)),
              u_new);
#else
    gmm::copy(gmm::sub_vector(U, gmm::sub_interval(0,nb_dof_pressure)),
              p_new);
    
    gmm::copy(gmm::sub_vector(U, gmm::sub_interval(nb_dof_pressure,nb_dof_displacement)),
              u_new);
#endif

    /*
     getfem::model modelOpening;
     modelOpening.add_initialized_fem_data("u", mf_u(), u_new);
     modelOpening.add_fem_variable("p", *mf_pressure_);
     getfem::add_source_term_generic_assembly_brick(modelOpening, *im_ls_, "((Xfem_plus(u))(2) - (Xfem_minus(u))(2))*Test_p", -1);
     modelOpening.assembly(getfem::model::BUILD_ALL);
     auto FF = modelOpening.real_rhs();
     std::cout << "LS1:" << std::endl;
     for(size_type i = 0; i < (*mf_pressure_).nb_dof(); ++i) {
     if (FF[i]!=0)
     std::cout << i << "\t" << FF[i] << std::endl;
     }
     
     */
    
    
    if (getfem::MPI_IS_MASTER())
        std::cout << "Solved problem in " << ftime - time << " seconds." << std::endl;

    return 1;

}

void problemMonolithic_nocrack::setBiotCoefficient (const scalar_type alpha){
    alpha_ = alpha;
}

void problemMonolithic_nocrack::setInflow (const scalar_type inflow){
    inflow_ = inflow;
}

void problemMonolithic_nocrack::setIntegrationMethod (const getfem::mesh_im_level_set &im){
    im_ = &im;
}

void problemMonolithic_nocrack::setLevelSetIntegrationMethod (const getfem::mesh_im_level_set &im) {
    im_ls_ = &im;
}

void problemMonolithic_nocrack::setFiniteElementPressure (const getfem::mesh_fem_level_set &mf){
    mf_pressure_ = &mf;
}

void problemMonolithic_nocrack::setFiniteElementDisplacement (const getfem::mesh_fem_sum &mf){
    mf_displacement_ = &mf;
}

void problemMonolithic_nocrack::setFiniteElementOpening (const getfem::mesh_fem &mf){
    mf_opening_ = &mf;
}

void problemMonolithic_nocrack::setFiniteElementRhs (getfem::mesh_fem &mf){
    mf_rhs = &mf;
}

void problemMonolithic_nocrack::setMeshLevelSet(const getfem::mesh_level_set &mls){
    mls_ = &mls;
}

void problemMonolithic_nocrack::setMesh (getfem::mesh &mesh){
    mesh_p_ = &mesh;
}

void problemMonolithic_nocrack::setTimeStep (const scalar_type &delta_t) {
    delta_t_ = delta_t;
}
