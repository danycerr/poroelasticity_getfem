//
//  crack_segment.h
//  
//
//  Created by Bianca Giovanardi on 09/04/15.
//
//

#ifndef ____crack_segment__
#define ____crack_segment__

#include "gmm/gmm.h"
#include "getfem/getfem_assembling.h"

using bgeot::scalar_type;
using bgeot::base_node;

class crack_segment {
    
public:
    scalar_type slope;
    scalar_type intercept;
    scalar_type x_left;
    scalar_type x_right;
    scalar_type length;
    
    base_node P_left;
    base_node P_right;

    crack_segment(const base_node P, const base_node Q);
    crack_segment(const base_node P, const base_node Q, bool ordered);
    ~crack_segment() = default;
    void print() const;
    bool getProjection (const base_node P, base_node &Q) const;
    base_node getProjection (const base_node P) const;
};

#endif /* defined(____crack_segment__) */
