//
//  crack_segment.cpp
//  
//
//  Created by Bianca Giovanardi on 09/04/15.
//
//

#include "crack_segment.h"

crack_segment::crack_segment(const base_node P, const base_node Q) {
    
    scalar_type xP = P[0];
    scalar_type xQ = Q[0];
    scalar_type yP = P[1];
    scalar_type yQ = Q[1];
    
    // We do not consider vertical fractures yet
    assert(xP != xQ);
    
    if(xP < xQ) {
        P_left = P;
        P_right = Q;
    }
    else {
        P_left = Q;
        P_right = P;
    }
    
    x_left = P_left[0];
    x_right = P_right[0];
    slope = (yP - yQ)/(xP - xQ);
    intercept = yQ - slope*xQ;
    
    length = sqrt((P_left[0]-P_right[0])*(P_left[0]-P_right[0])+(P_left[1]-P_right[1])*(P_left[1]-P_right[1]));
}

crack_segment::crack_segment(const base_node P, const base_node Q, bool ordered) {
    
    scalar_type xP = P[0];
    scalar_type xQ = Q[0];
    scalar_type yP = P[1];
    scalar_type yQ = Q[1];
    
    x_left = xP;
    x_right = xQ;
    slope = (yP - yQ)/(xP - xQ);
    intercept = yQ - slope*xQ;

    P_left = P;
    P_right = Q;
    
    length = sqrt((P_left[0]-P_right[0])*(P_left[0]-P_right[0])+(P_left[1]-P_right[1])*(P_left[1]-P_right[1]));

}

void crack_segment::print() const {
    std::cout << "Segment: tips (" << P_left[0] << ", " << P_left[1] << ") and (" << P_right[0] << ", " << P_right[1] << ")\n";
    std::cout << "Equation: \t y \t = \t" << slope << "\t x \t + \t " << intercept << std::endl;
}

bool crack_segment::getProjection (const base_node P, base_node &Q) const {

    bool projectionFallsInside = true;
    
    Q = this->getProjection(P);
    
    if (Q[0] < P_left[0] || Q[0] > P_right[0]) {
        //std::cout << "Warning: the projection of (" << x << ", " << y << ") is (" << x_projection << ", " << y_projection << ") and falls out of the segment. Considering segment" << std::endl;
        //print();
        projectionFallsInside = false;
    }
    
    return projectionFallsInside;
}

base_node crack_segment::getProjection (const base_node P) const {

    scalar_type x = P[0];
    scalar_type y = P[1];
    
    scalar_type x_projection = (x + slope*y - slope*intercept)/(slope*slope + 1.);
    scalar_type y_projection = (slope*x + slope*slope*y - slope*slope*intercept)/(slope*slope + 1.) + intercept;

    base_node Q(x_projection, y_projection);
    
    return Q;
}




