//
//  fracture.h
//  
//
//  Created by Bianca Giovanardi on 09/04/15.
//
//

#ifndef ____fracture__
#define ____fracture__

#include <list>
#include <vector>
#include "crack_segment.h"
#include "gmm/gmm.h"
#include "getfem/getfem_assembling.h"

using bgeot::scalar_type;
using bgeot::size_type;
using bgeot::base_node;
using bgeot::base_small_vector;
using std::list;
using std::vector;

typedef std::list<crack_segment>::iterator segment_iterator;
typedef std::list<crack_segment>::const_iterator segment_const_iterator;

class fracture {

private:    
    scalar_type cartesian_equation (const scalar_type x);
    
public:
    
    // The fracture is assumed to be a piecewise line y = (a_i x + b_i) I_{interval}
    vector<base_node> tips;              	// The tips of the fracture: tips[0] is the left one and tips[1] the right one.
    list<crack_segment> segments;           // The ordered list of segments which compose the fracture: the first is connected with the tip tips[0], and the last is connected with the tip tips[1]

    scalar_type averageSlope = 0.;
    
    void update_tips(void);
    
	// Adds a segement from the closest tip to point P and updates the tip.
	// If the new segment is aligned with the adjacent one, the two segments are merged. 
    void add_segment(const base_node P);
    
    void initialize_from_tips(const base_node P, const base_node Q);
	void print(void);    

    base_small_vector level_set_function(const base_node P);
    base_small_vector level_set_function_left(const base_node P);
    base_small_vector level_set_function_right(const base_node P);

    fracture() = default;
	fracture(const base_node P, const base_node Q);    
	~fracture() = default;
    
};

#endif /* defined(____fracture__) */
