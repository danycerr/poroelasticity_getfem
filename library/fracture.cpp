//
//  fracture.cpp
//  
//
//  Created by Bianca Giovanardi on 09/04/15.
//
//

#include "fracture.h"
#include <cmath>
#include <limits>

// This function is defined from -inf to +inf. Of course it represents the fracture just in the interval [tips[0][0], tips[1][0]]
scalar_type fracture::cartesian_equation (const scalar_type x) {
    
    scalar_type y;
    
    for (segment_iterator it = segments.begin(); it != segments.end(); ++it) {
        
        /*
         scalar_type a = it->slope;
         scalar_type b = it->intercept;
         scalar_type x_left = it->x_left;
         scalar_type x_right = it->x_right;
         
         y += (a*x + b)*(x > x_left && x < x_right);*/
        
        scalar_type x_left = it->x_left;
        scalar_type x_right = it->x_right;
        
        if (x >= x_left && x <= x_right) {
            scalar_type a = it->slope;
            scalar_type b = it->intercept;
            
            y = a*x + b;
        }
        
    }
    return y;

}

void fracture::update_tips(void) {
    
}

void fracture::add_segment(const base_node P){
    
    // Get the length at the previuos time
    scalar_type lengthN = 0.;
    for (segment_iterator it = segments.begin(); it != segments.end(); ++it) {
        lengthN += it->length;
    }
    
    scalar_type x = P[0];
   
    //assert(x <= (tips[0])[0] || x >= (tips[1])[0]);
    // I nuovi tip vengono calcolati approssimativamente, per cui può darsi che la frattura "torni indietro" anche se di pochissimo.
    // Questa tolleranza deve essere coerente con dist1 < 1e-6 || dist2 < 1e-6 perché in questo modo il tip viene solo spostato e
    // non vengono aggiunti ulteriori segmenti di frattura.
    //assert(x < (tips[0])[0] + 1e-6 || x > (tips[1])[0] - 1e-6);
    
    //scalar_type dist1 = sqrt(gmm::abs((P[0] - (tips[0])[0])*(P[0] - (tips[0])[0]) + (P[1] - (tips[0])[1])*(P[1] - (tips[0])[1])));
    //scalar_type dist2 = sqrt(gmm::abs((P[0] - (tips[1])[0])*(P[0] - (tips[1])[0]) + (P[1] - (tips[1])[1])*(P[1] - (tips[1])[1])));
    
    if (x < (tips[0])[0]) {
        // Add segment to the left!
        crack_segment segment(P, tips[0], 1);

		// In this case the new segment has the same slope as the adjacent one or if P is very close to a tip
		/*if (gmm::abs(segment.slope - segments.front().slope) < 1e-10 || dist1 < 1e-6 || dist2 < 1e-6) {
			segments.front().P_left = P;
			//segments.front().x_left = -std::numeric_limits<double>::infinity();
		}
		else{*/
			segments.front().x_left = segments.front().P_left[0];
			segment.x_left = -std::numeric_limits<double>::infinity();
        	segments.push_front(segment);
		//}
		tips[0] = P;
        
        scalar_type lengthNp1 = lengthN + segment.length;
        // BBBB In realtà dovrebbe essere la media pesata rispetto ai segmenti nella frattura
        averageSlope  = averageSlope*lengthN/lengthNp1 + segment.slope*(lengthNp1-lengthN)/lengthNp1;
    }
    else if (x > (tips[1])[0]){
        // Add segment to the right!
        crack_segment segment(tips[1], P, 1);

		// In this case the new segment has the same slope as the adjacent one or if P is very close to a tip
		/*if (gmm::abs(segment.slope - segments.back().slope) < 1e-10 || dist1 < 1e-6 || dist2 < 1e-6) {
			segments.back().P_right = P;
			//segments.back().x_right = x;
		}
		else{*/
			segments.back().x_right = segments.back().P_right[0];
			segment.x_right = std::numeric_limits<double>::infinity();
        	segments.push_back(segment);
		//}
		tips[1] = P;
        
        scalar_type lengthNp1 = lengthN + segment.length;
        // BBBB In realtà dovrebbe essere la media pesata rispetto ai segmenti nella frattura
        averageSlope  = averageSlope*lengthN/lengthNp1 + segment.slope*(lengthNp1-lengthN)/lengthNp1;
    }
}

void fracture::initialize_from_tips(const base_node P, const base_node Q) {

	scalar_type xP = P[0];
	scalar_type xQ = Q[0];

	assert(xP != xQ);

	tips.resize(2);    

    if (xP < xQ) {
        crack_segment segment(P, Q, 1);
		segment.x_left = -std::numeric_limits<double>::infinity();
		segment.x_right = std::numeric_limits<double>::infinity();
		tips[0] = P;
		tips[1] = Q;
    	segments.push_front(segment);
	}
    else { 
        crack_segment segment(Q, P, 1);
		segment.x_left = -std::numeric_limits<double>::infinity();
		segment.x_right = std::numeric_limits<double>::infinity();
		tips[0] = Q;
		tips[1] = P;
    	segments.push_front(segment);
	}
}

base_small_vector fracture::level_set_function(const base_node P){
    
    scalar_type x = P[0];
    scalar_type y = P[1];
    base_small_vector level_set(2); // The primary and secondary level sets
    
    vector<scalar_type> d_guess(segments.size());
    vector<scalar_type> x_guess(segments.size());
    
    size_type i = 0;
    for (segment_iterator it = segments.begin(); it != segments.end(); ++it) {
        scalar_type a = it->slope;
        scalar_type b = it->intercept;
        scalar_type x_left = it->x_left;
        scalar_type x_right = it->x_right;
        
        // The projection of (x, y) on the segment considered
        x_guess[i] = (x + a*y - a*b)/(a*a + 1);
        
        if(x_guess[i] < x_left)
            x_guess[i] = x_left;
        else if (x_guess[i] > x_right)
            x_guess[i] = x_right;
        
        scalar_type y_guess = a*x_guess[i] + b;
        
        d_guess[i] = sqrt((x - x_guess[i])*(x - x_guess[i]) + (y - y_guess)*(y - y_guess));
        
        ++i;
    }
    
    vector<scalar_type>::iterator d = min_element(d_guess.begin(), d_guess.end());
    
    if(y < this->cartesian_equation(x_guess[d - d_guess.begin()]))
        *d = -*d;
    
    level_set[0] = *d;
    //level_set[1] = (x-(tips[0])[0])*(x-(tips[1])[0]);
    
    scalar_type a_left = segments.front().slope;
    scalar_type b_left = segments.front().intercept;

    scalar_type a_right = segments.back().slope;
    scalar_type b_right = segments.back().intercept;
    
    scalar_type x_left = (tips[0])[0];
    scalar_type x_right = (tips[1])[0];
    
    level_set[1] = (a_left*y - (x_left - x) - a_left*a_left*x_left - a_left*b_left)*(a_right*y - (x_right - x) - a_right*a_right*x_right-a_right*b_right);
    
    return level_set;
}

base_small_vector fracture::level_set_function_left(const base_node P){
    
    scalar_type x = P[0];
    scalar_type y = P[1];
    base_small_vector level_set(2); // The primary and secondary level sets

    vector<scalar_type> d_guess(segments.size());
    vector<scalar_type> x_guess(segments.size());

	size_type i = 0;
    for (segment_iterator it = segments.begin(); it != segments.end(); ++it) {
        scalar_type a = it->slope;
        scalar_type b = it->intercept;
        scalar_type x_left = it->x_left;
        scalar_type x_right = it->x_right;

		// The projection of (x, y) on the segment considered
        x_guess[i] = (x + a*y - a*b)/(a*a + 1);

        if(x_guess[i] < x_left)
            x_guess[i] = x_left;
        else if (x_guess[i] > x_right)
            x_guess[i] = x_right;

        scalar_type y_guess = a*x_guess[i] + b;

        d_guess[i] = sqrt((x - x_guess[i])*(x - x_guess[i]) + (y - y_guess)*(y - y_guess));

		++i;
    }
    
    vector<scalar_type>::iterator d = min_element(d_guess.begin(), d_guess.end());

    if(y < this->cartesian_equation(x_guess[d - d_guess.begin()]))
        *d = -*d;
    
    level_set[0] = *d;
    //level_set[1] = -(x-(tips[0])[0]);

    scalar_type a_left = segments.front().slope;
    scalar_type b_left = segments.front().intercept;
    
    scalar_type x_left = (tips[0])[0];
    
    level_set[1] = -(a_left*y - (x_left - x) - a_left*a_left*x_left - a_left*b_left);
    
    return level_set;
}

base_small_vector fracture::level_set_function_right(const base_node P){
    
    scalar_type x = P[0];
    scalar_type y = P[1];
    base_small_vector level_set(2); // The primary and secondary level sets
    
    vector<scalar_type> d_guess(segments.size());
    vector<scalar_type> x_guess(segments.size());
    
    size_type i = 0;
    for (segment_iterator it = segments.begin(); it != segments.end(); ++it) {
        scalar_type a = it->slope;
        scalar_type b = it->intercept;
        scalar_type x_left = it->x_left;
        scalar_type x_right = it->x_right;
        
        // The projection of (x, y) on the segment considered
        x_guess[i] = (x + a*y - a*b)/(a*a + 1);
        
        if(x_guess[i] < x_left)
            x_guess[i] = x_left;
        else if (x_guess[i] > x_right)
            x_guess[i] = x_right;
        
        scalar_type y_guess = a*x_guess[i] + b;
        
        d_guess[i] = sqrt((x - x_guess[i])*(x - x_guess[i]) + (y - y_guess)*(y - y_guess));
        
        ++i;
    }
    
    vector<scalar_type>::iterator d = min_element(d_guess.begin(), d_guess.end());
    
    if(y < this->cartesian_equation(x_guess[d - d_guess.begin()]))
        *d = -*d;
    
    level_set[0] = *d;
    //level_set[1] = (x-(tips[1])[0]);
    
    scalar_type a_right = segments.back().slope;
    scalar_type b_right = segments.back().intercept;
    
    scalar_type x_right = (tips[1])[0];
    
    level_set[1] = (a_right*y - (x_right - x) - a_right*a_right*x_right-a_right*b_right);
    
    return level_set;
}


fracture::fracture(const base_node P, const base_node Q){
	this->initialize_from_tips(P, Q);
}

void fracture::print(void) {

		std::cout << "Crack tips:" << std::endl;
		std::cout << "(" << (tips[0])[0] << ", " << (tips[0])[1] << ") and (" << (tips[1])[0] << ", " << (tips[1])[1] << ")" << std::endl;

        std::cout << "Average slope: " << averageSlope << std::endl;

		std::cout << "Crack segments:" << std::endl;
    	for (segment_iterator it = segments.begin(); it != segments.end(); ++it) {
            std::cout << "slope " << it->slope << ", \t intercept " << it->intercept << ", \t left endpoint " << it->x_left << ", \t right endpoint " << it->x_right << "\t length of segment " << it->length << "." << std::endl;

		}

}
