//
//  problemPressure.h
//  
//
//  Created by Bianca Giovanardi on 22/06/16.
//
//

#ifndef ____problemMonolithic__
#define ____problemMonolithic__

#include "getfem/getfem_export.h" 
#include "getfem/getfem_mesh_im_level_set.h"
#include "getfem/getfem_mesh_fem_sum.h"
#include "getfem/getfem_mesh_fem_level_set.h"

/* Some Getfem++ types that we will be using: */
using bgeot::scalar_type;        /* = double */
using bgeot::size_type;          /* = unsigned long */
using bgeot::base_node;          /* geometrical nodes(derived from base_small_vector)*/
using bgeot::base_small_vector; /* special class for small (dim<16) vectors */

/* Definition of some matrix/vector types: */
typedef gmm::rsvector<scalar_type> sparse_vector_type;
typedef gmm::row_matrix<sparse_vector_type> sparse_matrix_type;

class problemMonolithic {

private:
    size_type N = 2;

    // Data (vectors for the finite element functions and scalar for the constants)
    const scalar_type viscosity_;                           // Viscosity
    const scalar_type permeabilityX_;                        // Permeability
    const scalar_type permeabilityY_;                        // Permeability
    scalar_type inflow_;
    const scalar_type lambda_, mu_;    	// Lame coefficients.
    scalar_type alpha_;            // Biot coefficient.
    scalar_type delta_t_;            // Biot coefficient.

    // Pointers to the integration methods
    const getfem::mesh_im_level_set *im_ = NULL;         // Integration method
    const getfem::mesh_im_level_set *im_ls_ = NULL;      // Integration method level set
    const getfem::mesh_level_set *mls_ = NULL;

    // Pointers to the finite elements methods
    const getfem::mesh_fem_level_set *mf_pressure_ = NULL;     // Finite elements method for pressure
    const getfem::mesh_fem_sum *mf_displacement_ = NULL;      // Finite elements method for displacement
    getfem::mesh_fem *mf_rhs = NULL;
    const getfem::mesh_fem *mf_opening_ = NULL;      // Finite elements method for displacement
    
    // Vectors containing the region flags of Dirichlet boundary and Neumann boundary.
    dal::bit_vector dirichletDofs1;
    dal::bit_vector dirichletDofs2;
    dal::bit_vector dirichletDofs3;
    
    // Global matrix, rhs, and solution for the algebraic system
    sparse_matrix_type A;
    std::vector<scalar_type> F;
    size_type nb_dof_pressure;
    size_type nb_dof_displacement;
    
protected:
    // The mesh, its type, and the dimension of the domain
    //! @cond
    getfem::mesh *mesh_p_ = NULL;                // Pointer to the mesh
    //! @endcond
    
public:
    //! @name Constructor and Destructor
    //@{
    
    //! \brief The constructor.
    /**
     *  @param viscosity        Viscosity
     *  @param permeability     Permeability
     *  @param opening          Opening
     */
    
    problemMonolithic( const scalar_type &viscosity,
                            const scalar_type &permeabilityX,
                            const scalar_type &permeabilityY,
                            const scalar_type &inflow,
                            const scalar_type &lambda,
                            const scalar_type &mu,
                            const scalar_type &alpha):
    viscosity_(viscosity), permeabilityX_(permeabilityX), permeabilityY_(permeabilityY), inflow_(inflow),
    lambda_(lambda), mu_(mu), alpha_(alpha)
    {};
    
    //! \brief The default destructor.
    ~problemMonolithic() = default;
    
    const getfem::mesh_fem_sum& mf_u() { return *mf_displacement_; }

    //! @name Public setters
    //@{
    void setBiotCoefficient (const scalar_type alpha);
    void setInflow (const scalar_type inflow);
    //! \brief Sets the integration method.
    void setIntegrationMethod (const getfem::mesh_im_level_set &im);
    //! \brief Sets the integration method.
    void setLevelSetIntegrationMethod (const getfem::mesh_im_level_set &im);
    //! \brief Sets the finite element space for pressure.
    void setFiniteElementPressure (const getfem::mesh_fem_level_set &mf);
    //! \brief Sets the finite element space for displacement.
    void setFiniteElementDisplacement (const getfem::mesh_fem_sum &mf);
    //! \brief Sets the finite element space for displacement.
    void setFiniteElementOpening (const getfem::mesh_fem &mf);
    void setFiniteElementRhs(getfem::mesh_fem &mf);
    void setMeshLevelSet(const getfem::mesh_level_set &mls);
    //! \brief Sets the type of the mesh (w.r.t. the getfem classification of geometric transformations) and the mesh itself.
    void setMesh (getfem::mesh &mesh);
    //! \brief Sets the time step for the time iteration.
    void setTimeStep (const scalar_type &delta_t);
    //@}
    
    bool getInitialDisplacement(std::vector<scalar_type> & u_new, const std::vector<scalar_type> & pressurePrev, const std::vector<scalar_type> load_top, const std::string BC_TYPE_TOP, const std::vector<scalar_type> load_bottom, const std::string BC_TYPE_BOT);
    
    bool solveAnalytic(std::vector<scalar_type> &p_new,
                                          std::vector<scalar_type> & u_new,
                                          const std::vector<scalar_type> & pressureAnalyticPrev,
                                          const std::vector<scalar_type> & openingAnalyticPrev,
                                          const std::vector<scalar_type> & openingDerivativeAnalyticPrev,
                                          const std::vector<scalar_type> load_top,
                                          const std::string BC_TYPE_TOP,
                                          const std::vector<scalar_type> load_bottom,
                                          const std::string BC_TYPE_BOT,
                                          const std::string lag = "");
        
    bool solve(std::vector<scalar_type> &p_new, std::vector<scalar_type> & u_new, const std::vector<scalar_type> & pressurePrev, const std::vector<scalar_type> & displacementPrev, const std::vector<scalar_type> & pressureOld, const std::vector<scalar_type> & displacementOld, const bool Static, const std::vector<scalar_type> &source, const std::string DISPLACEMENT_TYPE, const std::vector<scalar_type> load_top, const std::string BC_TYPE_TOP, const std::vector<scalar_type> load_bottom, const std::string BC_TYPE_BOT, const std::string lag = "");
    
    //! \brief This function initializes the boundary conditions associated to the system.
    void initialize(void);
    void initializeWithArtificialDirichletBC(const base_node artificialNode);
};

#endif /* defined(____problemMonolithic__) */
