L = 60;
h = L*0.1;

Point(1) = {0, -0.5*L,  0, h} ;
Point(2) = {0, 0.5*L,  0, h} ;
Point(3) = {L, 0.5*L,  0, h} ;
Point(4) = {L, -0.5*L,  0, h} ;

Point(5) = {0, 0,  0, h} ;

Line(1) = {1,5} ;
Line(2) = {5,2} ;
Line(3) = {2,3} ;
Line(4) = {3,4} ;
Line(5) = {4,1} ;

Line Loop(10) = {1,2,3,4,5} ;

// We can then define the surface as a list of line loops (only one
// here, since there are no holes--see `t4.geo'):

Plane Surface(13) = {10} ;

Physical Line(101) = {1,2} ;
Physical Line(102) = {3} ;
Physical Line(103) = {4} ;
Physical Line(104) = {5} ;

Physical Surface(14) = {13};