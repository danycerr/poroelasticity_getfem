L = 60;
h = L*0.0005; //L*0.001;
H = L*0.02;

Point(1) = {0, -0.5*L,  0, H} ;
Point(2) = {0, 0.5*L,  0, H} ;
Point(3) = {L, 0.5*L,  0, H} ;
Point(4) = {L, -0.5*L,  0, H} ;

Point(5) = {0, -0.01*L,  0, h} ;
Point(6) = {0, 0.01*L,  0, h} ;
Point(9) = {0, 0,  0, h} ;
Point(7) = {0.1*L, 0.01*L,  0, h} ;
Point(8) = {0.1*L, -0.01*L,  0, h} ;

Line(1) = {1,5} ;
Line(2) = {5,9} ;
Line(10) = {9,6} ;
Line(3) = {6,2} ;
Line(4) = {2,3} ;
Line(5) = {3,4} ;
Line(6) = {4,1} ;

Line(7) = {5,8} ;
Line(8) = {8,7} ;
Line(9) = {7,6} ;

Line Loop(10) = {1,2,10,3,4,5,6} ;

// We can then define the surface as a list of line loops (only one
// here, since there are no holes--see `t4.geo'):

Plane Surface(13) = {10} ;

Physical Line(101) = {1,2,10,3} ;
Physical Line(102) = {4} ;
Physical Line(103) = {5} ;
Physical Line(104) = {6} ;

Line {7} In Surface {13};
Line {8} In Surface {13};
Line {9} In Surface {13};

Physical Surface(14) = {13};