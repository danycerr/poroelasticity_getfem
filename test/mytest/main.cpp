//
//  main.cpp
//
//
//  Created by Bianca Giovanardi on 22/06/16.
//
//

#include "getfem/getfem_export.h"   /* export functions (save solution in a file)  */
#include "getfem/getfem_regular_meshes.h"
#include "getfem/getfem_import.h"
#include "getfem/getfem_mesh_level_set.h"
#include "getfem/getfem_mesh_fem_global_function.h"
#include "getfem/getfem_mesh_fem_level_set.h"
#include "getfem/getfem_mesh_im_level_set.h"
#include "getfem/getfem_mesh_fem_product.h"
#include "getfem/getfem_model_solvers.h"
#include "gmm/gmm.h"
#include "../../library/problemMonolithic.h"
#include "../../library/crack_segment.h"
#include "../../library/fracture.h"
#include <sstream>

#define START_FROM_GUESS 1

/* some Getfem++ types that we will be using */
using bgeot::scalar_type;        /* = double */
using bgeot::size_type;          /* = unsigned long */
using bgeot::base_node;          /* geometrical nodes(derived from base_small_vector)*/
using bgeot::dim_type;

typedef getfem::model_real_plain_vector  plain_vector;

double dist(const base_node &node1, const base_node &node2) {
    return gmm::sqrt((node1[0]-node2[0])*(node1[0]-node2[0]) +  (node1[1]-node2[1])*(node1[1]-node2[1]));
}

void printNode(const base_node &node) {
    std::cout << "(" << node[0] << ", " << node[1] << ")\t";
}

scalar_type norm2(std::vector<scalar_type> v) {
    
    scalar_type norm = 0.;
    for (int i = 0; i < v.size(); ++i) {
        norm += v[i]*v[i];
    }
    return gmm::sqrt(norm);
}

scalar_type error2(std::vector<scalar_type> u, std::vector<scalar_type> v) {
    scalar_type norm = 0.;
    for (int i = 0; i < v.size(); ++i) {
        norm += (u[i]-v[i])*(u[i]-v[i]);
    }
    return gmm::sqrt(norm);
}

class KGDSemiAnalyticDatumFromCSV
{
public:
    /**
     * Constructor
     * @param[in] fileName name of the file containing the semi-analytic solution
     */
    KGDSemiAnalyticDatumFromCSV(std::string fileName, int numberOfLineToRead) : _fileName(fileName), _numberOfLineToRead(numberOfLineToRead) {}
    
    /**
     * Destructor
     */
    ~KGDSemiAnalyticDatumFromCSV() {}
    
private:
    /**
     * Disabled meta-methods
     */
    KGDSemiAnalyticDatumFromCSV();
    KGDSemiAnalyticDatumFromCSV(KGDSemiAnalyticDatumFromCSV const &);
    KGDSemiAnalyticDatumFromCSV & operator=(KGDSemiAnalyticDatumFromCSV const &);
    
    /**
     * Given position, returns the index such that position is in the interval ( _sampledX[index], _sampledX[index+1] )
     */
    int locatePositionAlongCrack(scalar_type const abscissa) const
    {
        // If position is out of the bounds of the crack, throw an error
        if (abscissa < _sampledX[0] || abscissa > _sampledX[_sampledX.size()-1])
        {
            // say something useful
            std::cout << "Cannot interpolate the opening at point s=" << abscissa << ", because it is out of the crack limits.\n" << std::endl;
            std::cout << "The crack goes from s=" << _sampledX[0] << " to s=" << _sampledX[_sampledX.size()-1] << ".\n" << std::endl;
            return -1;
        }
        
        // Otherwise, look for the first index such that position is greater than _sampledX[i]
        int i =0;
        while(abscissa >= _sampledX[i])
        {
            ++i;
        }
        return i-1;
    }
    
public:
    /**
     *
     */
    scalar_type get(scalar_type const & x) const
    {
        // Find the index of the left hand point of the interval that includes the abscissa
        int index = locatePositionAlongCrack(x);
        scalar_type interpolatedDatum;
        if (index == -1)
            interpolatedDatum = 0.;
        else {
            // Perform a linear interpolation with the data available to the class
            interpolatedDatum = _sampledDatum[index] + (_sampledDatum[index+1] - _sampledDatum[index])*(x-_sampledX[index])/(_sampledX[index+1]-_sampledX[index]);
        }
        // Print the interpolated opening
        //std::cout << "x: " << x << "\t Datum: " << interpolatedDatum << std::endl;
        return interpolatedDatum;
    }
    
    /*
     * Prints the vector _sampledDatum
     */
    void printDatum() const
    {
        std::cout << "Printing the values of Datum(x):" << std::endl;
        for(int i=0; i<_sampledX.size(); ++i)
        {
            std::cout << "Datum(" << _sampledX[i] << ") \t = \t " << _sampledDatum[i] << std::endl;
        }
    }
    
    /*
     * Initializer that loads the opening of the semi-analytic solution
     */
    void init() {
        
        _sampledX.push_back(0);
        _sampledDatum.push_back(0);
        std::cout << "Reading input file... " << std::endl;
        std::ifstream inputFile(_fileName.c_str());
        
        std::string s;
        getline(inputFile, s);
        std::istringstream ss_x(s);
        while (ss_x)
        {
            if(!getline(ss_x, s, ',')) break;
            _sampledX.push_back(atof(s.c_str()));
        }
        
        int count = 0;
        while (count < _numberOfLineToRead-1)
        {
            getline(inputFile, s);
            count++;
        }
        
        getline(inputFile, s);
        std::istringstream ss(s);
        while (ss)
        {
            if(!getline(ss, s, ',')) break;
            _sampledDatum.push_back(atof(s.c_str()));
        }
        _sampledDatum[0] = _sampledDatum[1];
    }
    
private:
    
    /**
     * A sample of points along the crack
     */
    std::vector<scalar_type> _sampledX;
    
    /**
     * The value of the datum on the sample of points _sampleX
     */
    std::vector<scalar_type> _sampledDatum;
    
    /**
     * The name of the CSV file containing the semi-analytic solution for a sample of points
     */
    std::string _fileName;
    
    /**
     * The number of the line of the CSV file corresponding to the datum we want to read
     * (Line 0 should always contain x; Line _numberOfLineToRead contains datum(x))
     */
    int _numberOfLineToRead;
};

int main(int argc, char *argv[]) {
    
    double initialTime;
#ifdef GMM_USES_MPI
    initialTime = MPI_Wtime();
#else
    initialTime = gmm::uclock_sec();
#endif
    
    // ---------------------------------------------------------------------------------------------
    // ------------------------ IMPORTING THE PARAMETERS -------------------------------------------
    // ---------------------------------------------------------------------------------------------
    
    // Read parameters from file .param
    bgeot::md_param PARAM;
    PARAM.read_command_line(argc, argv);
    
    // The physical parameters
    const scalar_type lambda = PARAM.real_value("LAMBDA", "1st Lame parameter [Pa]");
    const scalar_type mu = PARAM.real_value("MU", "2nd Lame parameter [Pa]");
    const scalar_type viscosity = PARAM.real_value("VISCOSITY", "Fluid viscosity");
    const scalar_type permeability = PARAM.real_value("PERMEABILITY", "Matrix permeability");
    const scalar_type inflow = PARAM.real_value("INFLOW_BC", "Inflow boundary condition");
    
    // The numerical parameters
    const scalar_type enr_area_radius = PARAM.real_value("RADIUS_ENR_AREA", "Radius of the enrichment area");
    std::string vtkPath = PARAM.string_value("VTK_PATH", "The path to save VTK results in");
    const scalar_type dt = PARAM.real_value("deltaT", "Time step [s]");
    const int Nt = PARAM.real_value("N_ITER", "Number of time iterations [-]");
    const int Kmax = PARAM.real_value("N_FIXED_POINT_MAX", "Maximum number of FPI [-]");
    const int plotAt = PARAM.real_value("PLOT_AT", "Export once every PLOT_AT time iterations [-]");
    const scalar_type tol = PARAM.real_value("TOL", "Tolerance for FPI [-]");

    // The XFEM mesh
    const std::string MESH_FILE_XFEM = PARAM.string_value("MESH_FILE_XFEM", "Which mesh file to import for the Xfem.");
    const std::string MESH_TYPE = PARAM.string_value("MESH_TYPE","Mesh type ");
    bgeot::pgeometric_trans pgt = bgeot::geometric_trans_descriptor(MESH_TYPE);
    size_type N = pgt->dim();
    
    getfem::mesh meshXFEM;
    getfem::import_mesh(MESH_FILE_XFEM, meshXFEM);
    
    {
        // Export the mesh in .vtk
        getfem::vtk_export vtkMesh(vtkPath + "MeshXFEM.vtk");
        vtkMesh.exporting(meshXFEM);
        vtkMesh.write_mesh();
    }
    
    const scalar_type X_max = PARAM.real_value("X_MAX");
    
    // ---------------------------------------------------------------------------------------------
    // ------------------------ SETTING THE FINITE ELEMENTS ----------------------------------------
    // ---------------------------------------------------------------------------------------------
    
    // The FEMs
    const std::string FEM_TYPE_DISPLACEMENT = PARAM.string_value("FEM_TYPE_DISPLACEMENT","Fem (displacement)");
    const std::string FEM_TYPE_PRESSURE = PARAM.string_value("FEM_TYPE_PRESSURE","Fem (pressure)");
    
    // The integration methods
    const std::string INTEGRATION = PARAM.string_value("INTEGRATION", "Integration method");
    const std::string SIMPLEX_INTEGRATION = PARAM.string_value("SIMPLEX_INTEGRATION", "Name of simplex integration method");
    const std::string SINGULAR_INTEGRATION = PARAM.string_value("SINGULAR_INTEGRATION");
    
    // Setting the regular integration method for the pressure
    getfem::pintegration_method ppi = getfem::int_method_descriptor(INTEGRATION);
    getfem::mesh_im im_reg(meshXFEM);
    im_reg.set_integration_method(meshXFEM.convex_index(), ppi);
    
    // Setting the finite elements for the rhs
    getfem::pfem pf_displacement = getfem::fem_descriptor(FEM_TYPE_DISPLACEMENT);
    getfem::mesh_fem mf_rhs(meshXFEM);
    mf_rhs.set_finite_element(meshXFEM.convex_index(), pf_displacement);
    
    // Setting the finite elements for the pressure
    getfem::pfem pf_pressure = getfem::fem_descriptor(FEM_TYPE_PRESSURE);
    getfem::mesh_fem mf_pressure(meshXFEM);
    mf_pressure.set_finite_element(meshXFEM.convex_index(), pf_pressure);
    size_type pressure_dofs = mf_pressure.nb_dof();
    
    // Setting the finite elements for the opening
    getfem::mesh_fem mf_opening(meshXFEM);
    mf_opening.set_finite_element(meshXFEM.convex_index(), pf_displacement);
    size_type opening_dofs = mf_opening.nb_dof();
    
    // ---------------------------------------------------------------------------------------------
    // ------------------------ SUMMARY OUTPUT -----------------------------------------------------
    // ---------------------------------------------------------------------------------------------
    
    // Summary output
    std::cout << std::endl;
    std::cout << "FINITE ELEMENT DISPLACEMENT  = " << FEM_TYPE_DISPLACEMENT << std::endl;
    std::cout << "FINITE ELEMENT PRESSURE      = " << FEM_TYPE_PRESSURE << std::endl;
    std::cout << "INTEGRATION                  = " << INTEGRATION << std::endl;
    
    size_type nb_cv = 0;
    dal::bit_vector nn = meshXFEM.convex_index();
    
    bgeot::size_type i_cv;
    for (i_cv << nn; i_cv != bgeot::size_type(-1); i_cv << nn)
        nb_cv++;
    
    std::cout << "Mesh for XFEM: " << std::endl;
    std::cout << "# ELEMENTS: " << nb_cv << std::endl;
    std::cout << "# VERTICES: " << (meshXFEM.points()).size() << std::endl;
    std::cout << std::endl;
    
    // ---------------------------------------------------------------------------------------------
    // ------------------------ INITIAL FRACTURE ----------------------------------------------
    // ---------------------------------------------------------------------------------------------
    
    scalar_type crackLength = 9;
    std::string fileOpening("length9.csv");

    base_node P(0, 0);
    base_node Q(crackLength, 0);
    fracture frac(P,Q);

    // ---------------------------------------------------------------------------------------------
    // ------------------------ READING ANALYTIC DATA ---------------------------------------------
    // ---------------------------------------------------------------------------------------------
    
    std::vector<scalar_type> analyticPressure;
    gmm::resize(analyticPressure, pressure_dofs); gmm::clear(analyticPressure);
    
    std::vector<scalar_type> analyticHalfOpening;
    gmm::resize(analyticHalfOpening, opening_dofs); gmm::clear(analyticHalfOpening);
    
    std::vector<scalar_type> analyticOpening;
    gmm::resize(analyticOpening, opening_dofs); gmm::clear(analyticOpening);
    
    std::vector<scalar_type> analyticOpeningDerivative;
    gmm::resize(analyticOpeningDerivative, opening_dofs); gmm::clear(analyticOpeningDerivative);
    
    KGDSemiAnalyticDatumFromCSV kgdPressure(fileOpening,3);
    kgdPressure.init();
    
    KGDSemiAnalyticDatumFromCSV kgdOpening(fileOpening,1);
    kgdOpening.init();
    
    KGDSemiAnalyticDatumFromCSV kgdOpeningDerivative(fileOpening,2);
    kgdOpeningDerivative.init();
    
    for (size_type i = 0; i < opening_dofs; ++i) {
        scalar_type x = mf_opening.point_of_basic_dof(i)[0];
        
        if(x<crackLength) {
            analyticOpeningDerivative[i] = kgdOpeningDerivative.get(x);
        }
        else {
            analyticOpeningDerivative[i] = 0.;
        }
    }

    for (size_type i = 0; i < pressure_dofs; ++i) {
        scalar_type x = mf_pressure.point_of_basic_dof(i)[0];
        
        if(x<crackLength) {
            analyticPressure[i] = (kgdPressure.get(x) > 0)*kgdPressure.get(x);
        }
        else {
            analyticPressure[i] = 0.;
        }
    }
    
    for (size_type i = 0; i < opening_dofs; ++i) {
        scalar_type x = mf_opening.point_of_basic_dof(i)[0];
        
        if(x<crackLength) {
            analyticOpening[i] = kgdOpening.get(x);
            analyticHalfOpening[i] = 0.5*analyticOpening[i];
        }
        else {
            analyticOpening[i] = 0.;
            analyticHalfOpening[i] = 0.;
        }
    }
    
    {
        std::string vtkFilename;
        
        // Export the analytic solution in .vtk
        vtkFilename = vtkPath + "AnalyticOpening.vtk";
        std::cout << "Exporting analytic opening in " << vtkFilename << std::endl;
        getfem::vtk_export vtkOpening(vtkFilename);
        vtkOpening.exporting(mf_opening);
        vtkOpening.write_mesh();
        vtkOpening.write_point_data(mf_opening, analyticHalfOpening, "AnalyticHalfOpening");
        vtkOpening.write_point_data(mf_opening, analyticOpeningDerivative, "AnalyticOpeningDerivative");
        
        // Export the analytic solution in .vtk
        vtkFilename = vtkPath + "AnalyticPressure.vtk";
        std::cout << "Exporting analytic pressure in " << vtkFilename << std::endl;
        getfem::vtk_export vtkAnPress(vtkFilename);
        vtkAnPress.exporting(mf_pressure);
        vtkAnPress.write_mesh();
        vtkAnPress.write_point_data(mf_pressure, analyticPressure, "AnalyticPressure");
        
    }
    
    // ---------------------------------------------------------------------------------------------
    // ------------------------ SETTING THE FEMs AND IMs FOR XFEM ----------------------------------
    // ---------------------------------------------------------------------------------------------

    getfem::mesh_fem mf_displacement_qdim1(meshXFEM);
    mf_displacement_qdim1.set_finite_element(meshXFEM.convex_index(), pf_displacement);
    
    getfem::mesh_level_set              mls(meshXFEM);
    getfem::mesh_im_level_set           im_all_level_set(mls);
    getfem::mesh_fem_level_set          mfls_u(mls, mf_displacement_qdim1);
    getfem::mesh_fem_global_function    mf_sing_u_right(meshXFEM);
    getfem::mesh_fem                    mf_partition_of_unity(meshXFEM);
    getfem::mesh_fem_product            mf_product_right(mf_partition_of_unity, mf_sing_u_right);
    getfem::mesh_fem_sum                mf_u_sum(meshXFEM);
    getfem::level_set                   ls(meshXFEM, 1, true);
    getfem::level_set                   ls_right(meshXFEM, 1, true);

    mf_u_sum.set_qdim(dim_type(N));
    getfem::pintegration_method simp_ppi = getfem::int_method_descriptor(SIMPLEX_INTEGRATION);
    getfem::pintegration_method sing_ppi = (SINGULAR_INTEGRATION.size() ?
                                            getfem::int_method_descriptor(SINGULAR_INTEGRATION) : 0);
    
    im_all_level_set.set_integration_method(meshXFEM.convex_index(), ppi);
    mls.add_level_set(ls);
    im_all_level_set.set_simplex_im(simp_ppi, sing_ppi);
    mf_partition_of_unity.set_classical_finite_element(1);
    
    ls.reinit();
    std::cout << "ls.get_mesh_fem().nb_dof() = " << (ls.get_mesh_fem()).nb_dof() << "\n";
    for (size_type d = 0; d < ls.get_mesh_fem().nb_basic_dof(); ++d) {
        ls.values(0)[d] = frac.level_set_function(ls.get_mesh_fem().point_of_basic_dof(d))[0];
        ls.values(1)[d] = frac.level_set_function(ls.get_mesh_fem().point_of_basic_dof(d))[1];
    }
    ls.touch();
    
    ls_right.reinit();
    std::cout << "ls_right.get_mesh_fem().nb_dof() = " << (ls_right.get_mesh_fem()).nb_dof() << "\n";
    for (size_type d = 0; d < ls_right.get_mesh_fem().nb_basic_dof(); ++d) {
        ls_right.values(0)[d] = frac.level_set_function_right(ls_right.get_mesh_fem().point_of_basic_dof(d))[0];
        ls_right.values(1)[d] = frac.level_set_function_right(ls_right.get_mesh_fem().point_of_basic_dof(d))[1];
    }
    ls_right.touch();
    
    mls.adapt();
    im_all_level_set.adapt();
    mfls_u.adapt();
    
    // Tip enrichment
    std::cout << "Setting up the singular functions for the enrichment on the RHS tip\n";
    std::vector<getfem::pglobal_function> vfunc_right(4);
    for (unsigned i = 0; i < vfunc_right.size(); ++i) {
        // use the singularity
        auto s = std::make_shared<getfem::crack_singular_xy_function>(16+i);
        vfunc_right[i] = getfem::global_function_on_level_set(ls_right, s);
    }
    mf_sing_u_right.set_functions(vfunc_right);
    dal::bit_vector enriched_dofs_right;
    
    plain_vector X(mf_partition_of_unity.nb_dof());
    plain_vector Y(mf_partition_of_unity.nb_dof());
    
    getfem::interpolation(ls_right.get_mesh_fem(), mf_partition_of_unity,
                          ls_right.values(1), X);
    getfem::interpolation(ls_right.get_mesh_fem(), mf_partition_of_unity,
                          ls_right.values(0), Y);
    for (size_type j = 0; j < mf_partition_of_unity.nb_dof(); ++j) {
        if (gmm::sqr(X[j]) + gmm::sqr(Y[j]) <= gmm::sqr(enr_area_radius)) {
            //std::cout << "Dof " << j << " enriched... (right) " << std::endl;
            //scalar_type x = mf_partition_of_unity.point_of_basic_dof(j)[0];
            //scalar_type y = mf_partition_of_unity.point_of_basic_dof(j)[1];
            //std::cout << j << "\t" << x << "\t" << y << std::endl;
            enriched_dofs_right.add(j);
        }
    }
    
    std::cout << "There are " << enriched_dofs_right.card() << " enriched dofs for the RHS crack tip" << std::endl;
    
    mf_product_right.set_enrichment(enriched_dofs_right);
    
    // Sum the Heaviside enrichment with the tip enrichment
    mf_u_sum.set_mesh_fems(mf_product_right, mfls_u);
    
    size_type enriched_displacement_dofs = mf_u_sum.nb_dof();
    
    // Integration method to integrate ON the crack Gamma
    getfem::mesh_im_level_set im_on_level_set(mls, getfem::mesh_im_level_set::INTEGRATE_BOUNDARY);
    im_on_level_set.set_integration_method(meshXFEM.convex_index(), ppi);
    im_on_level_set.set_simplex_im(simp_ppi);
    im_on_level_set.adapt();

    // Enriched space for pressure
    getfem::mesh_fem_level_set  mfls_pressure(mls, mf_pressure);
    mfls_pressure.adapt();
    size_type enriched_pressure_dofs = mfls_pressure.nb_dof();

    getfem::mesh mcut;
    mls.global_cut_mesh(mcut);
    getfem::mesh_fem mf_cut(mcut);
    getfem::pfem pf_cut = getfem::fem_descriptor("FEM_PK(2, 1)");
    mf_cut.set_finite_element(mcut.convex_index(), pf_cut);
    
    // ---------------------------------------------------------------------------------------------
    // ---------------------------- MONOLITHIC PROBLEM DEFINITION ----------------------------------
    // ---------------------------------------------------------------------------------------------
    
    scalar_type alpha = 0;
    problemMonolithic Mono(viscosity, permeability, permeability, 0.5*inflow, lambda, mu, alpha);
    Mono.setMesh(meshXFEM);
    Mono.setIntegrationMethod(im_all_level_set);
    Mono.setLevelSetIntegrationMethod(im_on_level_set);
    Mono.setFiniteElementPressure(mfls_pressure);
    Mono.setFiniteElementDisplacement(mf_u_sum);
    Mono.setFiniteElementOpening(mf_opening);
    Mono.setFiniteElementRhs(mf_rhs);
    Mono.setMeshLevelSet(mls);
    Mono.setTimeStep(dt);

    const std::string LOAD_TYPE_TOP = "Stress";
    std::vector<scalar_type> load_top(2);
    load_top[0] = 0;
    load_top[1] = 0;
    
    const std::string LOAD_TYPE_BOT = "Stress";
    std::vector<scalar_type> load_bot(2);
    load_bot[0] = 0;
    load_bot[1] = 0;


    if (!(LOAD_TYPE_TOP == "Stress" && LOAD_TYPE_BOT == "Stress")) {
        Mono.initialize();
    }
    else {
        base_node artificialDBC(X_max,0);
        Mono.initializeWithArtificialDirichletBC(artificialDBC);
    }
    
    std::vector<scalar_type> u_new;
    gmm::resize(u_new, enriched_displacement_dofs); gmm::clear(u_new);
    std::vector<scalar_type> u_prev;
    gmm::resize(u_prev, opening_dofs); gmm::clear(u_prev);

    std::vector<scalar_type> p_new;
    gmm::resize(p_new, enriched_pressure_dofs); gmm::clear(p_new);
    std::vector<scalar_type> p_prev;
    gmm::resize(p_prev, enriched_pressure_dofs); gmm::clear(p_prev);
    
    for (size_type i = 0; i < opening_dofs; ++i) {
        u_prev[i] = 0.001;
    }
    
    for (size_type i = 0; i < enriched_pressure_dofs; ++i) {
        p_prev[i] = 1.e6;
    }
    
    std::cout << "\nInitial condition:\n" << std::endl;
    
#if START_FROM_GUESS
    Mono.solveAnalytic(p_new, u_new, p_prev, u_prev, analyticOpeningDerivative, load_top, LOAD_TYPE_TOP, load_bot, LOAD_TYPE_BOT, "8.57");
#else
    getfem::interpolation(mf_pressure, mfls_pressure, analyticPressurePrev, p_prev);
    Mono.solveAnalytic(p_new, u_new, p_prev, analyticOpening, analyticOpeningDerivative, load_top, LOAD_TYPE_TOP, load_bot, LOAD_TYPE_BOT,"8.57");
#endif
    
    getfem::mesh_fem mf_exp(mcut, mf_u_sum.get_qdim());
    mf_exp.set_classical_discontinuous_finite_element(2, 0.001);
    std::vector<scalar_type> u_new_exp;
    gmm::resize(u_new_exp, mf_exp.nb_dof()); gmm::clear(u_new_exp);
    getfem::interpolation(mf_u_sum, mf_exp, u_new, u_new_exp);
    
    getfem::mesh_fem mf_pres_exp(mcut);
    mf_pres_exp.set_classical_discontinuous_finite_element(2, 0.001);
    plain_vector p_new_exp(mf_pres_exp.nb_dof());
    getfem::interpolation(mfls_pressure, mf_pres_exp, p_new, p_new_exp);
    
    std::vector<scalar_type> u_old;
    gmm::resize(u_old, enriched_displacement_dofs); gmm::clear(u_old);
    std::vector<scalar_type> p_old;
    gmm::resize(p_old, enriched_pressure_dofs); gmm::clear(p_old);

    bool isStatic = 1;
    scalar_type error = 1.;
    int kk = 0;

    while (kk < Kmax && error > tol) {
        
        u_prev = u_new;
        p_prev = p_new;

        {
            std::string vtkFilename;
            std::ostringstream number;
            number << kk;
            
            getfem::interpolation(mf_u_sum, mf_exp, u_new, u_new_exp);
            
            // Export the displacement in .vtk
            vtkFilename = vtkPath + "Displacement.iter." + number.str() + ".vtk";
            std::cout << "Exporting solution (displacement) in " << vtkFilename << std::endl;
            getfem::vtk_export vtkDisp(vtkFilename);
            vtkDisp.exporting(mf_exp);
            vtkDisp.write_mesh();
            vtkDisp.write_point_data(mf_exp, u_new_exp, "Displacement");
            
            getfem::interpolation(mfls_pressure, mf_pres_exp, p_new, p_new_exp);
            
            // Export the pressure in .vtk
            vtkFilename = vtkPath + "Pressure.iter." + number.str() + ".vtk";
            std::cout << "Exporting pressure in " << vtkFilename << std::endl;
            getfem::vtk_export vtkPressure(vtkFilename);
            vtkPressure.exporting(mf_pres_exp);
            vtkPressure.write_mesh();
            vtkPressure.write_point_data(mf_pres_exp, p_new_exp, "Pressure");
        }
        
        std::cout << "\nIteration number:\t" << kk << std::endl;
        Mono.solve(p_new, u_new, p_prev, u_prev, p_old, u_old, isStatic, analyticOpeningDerivative, "NUMERIC", load_top, LOAD_TYPE_TOP, load_bot, LOAD_TYPE_BOT, "8.57");

        std::vector<scalar_type> diffDisp(enriched_displacement_dofs);
        for (size_type i = 0; i < enriched_displacement_dofs; ++i) {
            diffDisp[i] = u_new[i] - u_prev[i];
        }
        scalar_type errDisp = getfem::asm_H1_norm (im_all_level_set, mf_u_sum, diffDisp);
        scalar_type normDisp = getfem::asm_H1_norm (im_all_level_set, mf_u_sum, u_new);
        
        std::vector<scalar_type> diffPres(enriched_pressure_dofs);
        for (size_type i = 0; i < enriched_pressure_dofs; ++i) {
            diffPres[i] = p_new[i] - p_prev[i];
        }
        scalar_type errPres = getfem::asm_H1_norm (im_all_level_set, mfls_pressure, diffPres);
        scalar_type normPres = getfem::asm_H1_norm (im_all_level_set, mfls_pressure, p_new);
        
        error = gmm::sqrt(errDisp*errDisp/(normDisp*normDisp) + errPres*errPres/(normPres*normPres));
        
        std::cout << "\tTotal increment = " << error << std::endl;
        std::cout << "\t\tPressure increment = " << errPres*errPres/(normPres*normPres) << std::endl;
        std::cout << "\t\tDisplacement increment = " << errDisp*errDisp/(normDisp*normDisp) << std::endl;

        kk++;
    }

    // ---------------------------------------------------------------------------------------------
    // ------------------------ EXPORTING INITIAL SOLUTIONS ----------------------------------------
    // ---------------------------------------------------------------------------------------------
    
    {
        std::string vtkFilename;
        
        getfem::interpolation(mf_u_sum, mf_exp, u_new, u_new_exp);
        
        // Export the displacement in .vtk
        vtkFilename = vtkPath + "Displacement.0.vtk";
        std::cout << "Exporting solution (displacement) in " << vtkFilename << std::endl;
        getfem::vtk_export vtkDisp(vtkFilename);
        vtkDisp.exporting(mf_exp);
        vtkDisp.write_mesh();
        vtkDisp.write_point_data(mf_exp, u_new_exp, "Displacement");
        
        getfem::interpolation(mfls_pressure, mf_pres_exp, p_new, p_new_exp);

        // Export the pressure in .vtk
        vtkFilename = vtkPath + "Pressure.0.vtk";
        std::cout << "Exporting pressure in " << vtkFilename << std::endl;
        getfem::vtk_export vtkPressure(vtkFilename);
        vtkPressure.exporting(mf_pres_exp);
        vtkPressure.write_mesh();
        vtkPressure.write_point_data(mf_pres_exp, p_new_exp, "Pressure");
        
    }
    
    return 0;
}
