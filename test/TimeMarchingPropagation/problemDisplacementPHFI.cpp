//
//  problemFractureEvolution.cpp
//  
//
//  Created by Bianca Giovanardi on 01/10/14.
//
//

#include "gmm/gmm_inoutput.h"
#include "gmm/gmm_superlu_interface.h"
#include "problemDisplacementPHFI.h"
#include "getfem/getfem_assembling.h"
#include "getfem/getfem_model_solvers.h"

typedef getfem::model_real_plain_vector  plain_vector;

void problemDisplacementPHFI::solve(std::vector<scalar_type> &u_new, const std::vector<scalar_type> &c_k_, const std::vector<scalar_type> &u_k_, const std::vector<scalar_type> &p_k_) {

    nb_dof_displacement = (*mf_disp_).nb_dof();
    gmm::resize(u_new, nb_dof_displacement); gmm::clear(u_new);

    // Model description.
    getfem::model model;

    // Main unknown of the problem.
    model.add_fem_variable("u", *mf_disp_);
    model.add_initialized_fem_data("c", *mf_frac_, c_k_);
    model.add_initialized_fem_data("uk", *mf_disp_, u_k_);
    model.add_initialized_fem_data("p", *mf_pres_, p_k_);
    
    // Elasticity brick
    model.add_initialized_fixed_size_data("lambda", plain_vector(1, lambda_lame_));
    model.add_initialized_fixed_size_data("mu", plain_vector(1, mu_lame_));
    model.add_initialized_fixed_size_data("eta", plain_vector(1, eta_eps_));
    getfem::add_linear_generic_assembly_brick(model, *im_, "(c*c + eta)*(lambda*Trace((Grad_u+Grad_u')/2)*Trace((Grad_Test_u+Grad_Test_u')/2)+2*mu*((Grad_u+Grad_u')/2):((Grad_Test_u+Grad_Test_u')/2))", -1);

    // 2(alpha-1) p c grad(c) .v
    //model.add_initialized_fixed_size_data("alpha", plain_vector(1, alpha_));
    //getfem::add_linear_generic_assembly_brick(model, *im_, "2*(alpha-1)*c*p*(Grad_c.Test_u)", -1);

    // alpha c*c grad(p) .v
    //getfem::add_linear_generic_assembly_brick(model, *im_, "alpha*c*c*(Grad_p.Test_u)", -1);

    getfem::add_Dirichlet_condition_with_penalization(model, *im_, "u", 1.e14, 107, "uk");

    double time;
#ifdef GMM_USES_MPI
    time = MPI_Wtime();
#else
    time = gmm::uclock_sec();
#endif

    // Generic solve.
    std::cout << "Total number of variables : " << model.nb_dof() << std::endl;
    gmm::iteration iter(1E-9, 1, 40000);
    getfem::standard_solve(model, iter);
    
    // Solution extraction
    gmm::copy(model.real_variable("u"), u_new);
    
    double ftime;
#ifdef GMM_USES_MPI
    ftime = MPI_Wtime();
#else
    ftime = gmm::uclock_sec();
#endif
    
    if (getfem::MPI_IS_MASTER())
        std::cout << "Linear system solved in " << ftime - time << " seconds." << std::endl;

}

void problemDisplacementPHFI::setIntegrationMethod (const getfem::mesh_im &im){
    im_ = &im;
}

void problemDisplacementPHFI::setFiniteElementDisplacement (const getfem::mesh_fem &mf){
    mf_disp_ = &mf;
}

void problemDisplacementPHFI::setFiniteElementFracture (const getfem::mesh_fem &mf){
    mf_frac_ = &mf;
}

void problemDisplacementPHFI::setFiniteElementPressure (const getfem::mesh_fem &mf){
    mf_pres_ = &mf;
}

void problemDisplacementPHFI::setMesh (getfem::mesh &mesh){
    mesh_p_ = &mesh;
}
