//
//  cluster.cpp
//  
//
//  Created by Bianca Giovanardi on 22/05/16.
//
//

#include "cluster.h"

cluster::cluster(const std::tuple < bgeot::base_node, scalar_type, scalar_type > element) {
    setOfElements.push_back(element);
}

void cluster::print() const {

    for (auto it = setOfElements.begin(); it != setOfElements.end(); ++it) {
        std::cout << "\t\t Node : \t";
        bgeot::base_node thisNode = std::get<0>(*it);
        std::cout << "(" << thisNode[0] << ", " << thisNode[1] << ")\t";
        std::cout << "\n\t\t c =  " << std::get<1>(*it)  << "\t";
        std::cout << "gradCr =  " << std::get<2>(*it);
        std::cout << std::endl;
    }

}

bool cluster::clusterShouldContainTuple (const std::tuple < bgeot::base_node, scalar_type, scalar_type > & tupleToTest, const scalar_type radius) const {

    bgeot::base_node node = std::get<0>(tupleToTest);
    
    for (auto it = setOfElements.begin(); it != setOfElements.end(); ++it) {

        bgeot::base_node tryNode = std::get<0>(*it);
        scalar_type distance = sqrt((tryNode[0]-node[0])*(tryNode[0]-node[0]) + (tryNode[1]-node[1])*(tryNode[1]-node[1]));

        if (distance < radius) {
            return true;
        }
    }
    
    return false;
}

void cluster::addElement (const std::tuple < bgeot::base_node, scalar_type, scalar_type > elementToAdd) {

    setOfElements.push_back(elementToAdd);
}

void cluster::computeCenter() {
    
    scalar_type x = 0;
    scalar_type y = 0;
    scalar_type sumGrad = 0;
    
    for (auto it = setOfElements.begin(); it != setOfElements.end(); ++it) {
        
        bgeot::base_node currentNode = std::get<0>(*it);
        scalar_type currentGradient = std::get<2>(*it);

        x += currentNode[0]*currentGradient;
        y += currentNode[1]*currentGradient;
        sumGrad += currentGradient;

    }
    
    x = x/sumGrad;
    y = y/sumGrad;

    center = bgeot::base_node(x, y);
}

bgeot::base_node cluster::getCenter() const {
    return center;
}

void cluster::setAverageOfC(const scalar_type average) {

    averagePhaseFieldOnSegment = average;
}

bool cluster::shouldBeMergedWithCluster(const cluster & myCluster, const scalar_type radius) const {
    
    for (auto itA = setOfElements.begin(); itA != setOfElements.end(); ++itA) {
        bgeot::base_node nodeA = std::get<0>(*itA);
        for (auto itB = myCluster.setOfElements.begin(); itB != myCluster.setOfElements.end(); ++itB) {
            bgeot::base_node nodeB = std::get<0>(*itB);
            scalar_type distance = sqrt((nodeA[0]-nodeB[0])*(nodeA[0]-nodeB[0]) + (nodeA[1]-nodeB[1])*(nodeA[1]-nodeB[1]));
            if (distance < radius)
                return true;
        }
    }

    return false;
}
