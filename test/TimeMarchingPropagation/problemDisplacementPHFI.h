//
//  problemJustDisplacement.h
//  
//
//  Created by Bianca Giovanardi on 01/10/14.
//
//

#ifndef ____problemDisplacementPHFI__
#define ____problemDisplacementPHFI__

#include "getfem/getfem_export.h" 

/* Some Getfem++ types that we will be using: */
using bgeot::scalar_type;        /* = double */
using bgeot::size_type;          /* = unsigned long */

/* Definition of some matrix/vector types: */
typedef gmm::rsvector<scalar_type> sparse_vector_type;
typedef gmm::row_matrix<sparse_vector_type> sparse_matrix_type;

/**
 * \class problemFractureEvolution
 * This class solves one Newton iteration, given the solution of the previous Newton iteration and that of the previous time iteration, of the following system:
 *
 *    \f$ \nabla \cdot \left( (\eta_{\epsilon} + c^2) (\lambda \text{tr}\mathbf{\varepsilon}(\mathbf{u}) \mathbf{I} + 2\mu \mathbf{\varepsilon}(\mathbf{u}) ) \right)  = \mathbf{0} \f$
 *
 *    \f$ \frac{1}{M} \dot{c} + c \mathbf{\varepsilon} \cdot \mathbf{\sigma} - \epsilon \Delta c - \frac{1}{\epsilon} (1 - c) = 0 \f$
 *
 *   TO BE COMPLETED WITH BOUNDARY CONDITIONS
 */

class problemDisplacementPHFI {

private:

    // Time step
    scalar_type delta_t_;

    // Data (vectors for the finite element functions and scalar for the constants)
    scalar_type eta_eps_;                           // Coefficient for residual stress
    scalar_type lambda_lame_;                       // 1st Lame' parameter
    scalar_type mu_lame_;                           // 2nd Lame' parameter
    scalar_type epsilon_;                           // Characteristic dimension of the fracture
    scalar_type G_resistance_;						// Resistance of the material
    scalar_type alpha_;                             // Biot paramenter
    
    // Pointers to the integration methods
    const getfem::mesh_im *im_ = NULL;      // Integration method
    
    // Pointers to the finite elements methods
    const getfem::mesh_fem *mf_disp_ = NULL;     // Finite elements method for displacement
    const getfem::mesh_fem *mf_frac_ = NULL;     // Finite elements method for fracture
    const getfem::mesh_fem *mf_pres_ = NULL;     // Finite elements method for pressure
    
    // Global matrix, rhs, and solution for the algebraic system
    sparse_matrix_type A;
    std::vector<scalar_type> F;
    size_type nb_dof_phaseField;
    size_type nb_dof_displacement;
    
protected:
    // The mesh, its type, and the dimension of the domain
    //! @cond
    getfem::mesh *mesh_p_ = NULL;                // Pointer to the mesh
    //! @endcond
    
public:
    //! @name Constructor and Destructor
    //@{
    
    //! \brief The constructor.
    /**
     *  @param eta_eps          Coefficient for residual stress
     *  @param lambda_lame      1st Lame' parameter
     *  @param mu_lame          2nd Lame' parameter
     *  @param epsilon          Characteristic dimension of the fracture
     *  @param G_resistance     Resistance of the material
     */
    
    problemDisplacementPHFI( const scalar_type &eta_eps,
                             const scalar_type &lambda_lame,
                             const scalar_type &mu_lame,
                             const scalar_type &epsilon,
                             const scalar_type &alpha,
                             const scalar_type &G_resistance):
    eta_eps_(eta_eps), lambda_lame_(lambda_lame),
    mu_lame_(mu_lame), epsilon_(epsilon), alpha_(alpha), G_resistance_(G_resistance) {};
    
    //! \brief The default destructor.
    ~problemDisplacementPHFI() = default;
    
    
    //! @name Public setters
    //@{
    //! \brief Sets the integration method for displacement.
    void setIntegrationMethod (const getfem::mesh_im &im);
    //! \brief Sets the finite element space for displacement.
    void setFiniteElementDisplacement (const getfem::mesh_fem &mf);
    //! \brief Sets the finite element space for fracture.
    void setFiniteElementFracture (const getfem::mesh_fem &mf);
    //! \brief Sets the finite element space for fracture.
    void setFiniteElementPressure (const getfem::mesh_fem &mf);
    //! \brief Sets the type of the mesh (w.r.t. the getfem classification of geometric transformations) and the mesh itself.
    void setMesh (getfem::mesh &mesh);
    //@}
        
    //! \brief This function solves the algebraic system built with the function assembly.
    void solve(std::vector<scalar_type> &u_new, const std::vector<scalar_type> &c_k_, const std::vector<scalar_type> &u_k_, const std::vector<scalar_type> &p_k_);
    
};

#endif /* defined(____problemDisplacementPHFI__) */
