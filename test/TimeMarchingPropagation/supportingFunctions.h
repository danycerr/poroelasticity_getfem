//
//  supportingFunctions.h
//
//
//  Created by Bianca Giovanardi on 26/03/15.
//
//

#ifndef _supportingFunctions_h
#define _supportingFunctions_h

#include <cmath>
#include "cluster.h"
#include "../../Library/fracture.h"
#include "getfem/getfem_regular_meshes.h"

/*
void organize_in_cluster(const std::tuple < bgeot::base_node, scalar_type, scalar_type > currentTuple, std::vector < cluster > & setOfClusters, const scalar_type radius) {

    for (auto it = setOfClusters.begin(); it != setOfClusters.end(); ++it) {

        if (it->clusterShouldContainTuple(currentTuple, radius)) {
            it->addElement(currentTuple);
            return;
        }
        
    }
    
    setOfClusters.push_back(cluster(currentTuple));
    
}*/

void organize_in_cluster(const bgeot::base_node &node, std::vector< std::vector< bgeot::base_node > >   &cluster, const scalar_type radius) {
/*
    if (cluster.size() == 0) {
        std::vector< bgeot::base_node > tip_nodes;
        tip_nodes.push_back(node);
        cluster.push_back(tip_nodes);
        return;
    }*/
    
    for (size_type i = 0; i < cluster.size(); ++i) {
        for (size_type j = 0; j < (cluster[i]).size(); ++j) {
            bgeot::base_node tryNode = (cluster[i])[j];
            scalar_type distance = sqrt((tryNode[0]-node[0])*(tryNode[0]-node[0]) + (tryNode[1]-node[1])*(tryNode[1]-node[1]));
            if (distance < radius) {
                cluster[i].push_back(node);
                return;
            }
        }
    }
    
    std::vector< bgeot::base_node > tip_nodes;
    tip_nodes.push_back(node);
    cluster.push_back(tip_nodes);
}

#endif
