//
//  problemFracturePHFI.cpp
//  
//
//  Created by Bianca Giovanardi on 01/10/14.
//
//

#include "gmm/gmm_inoutput.h"
#include "gmm/gmm_superlu_interface.h"
#include "problemFracturePHFI.h"
#include "getfem/getfem_model_solvers.h"

#define DIRICHLET_ON_INTERSECTION 1

typedef getfem::model_real_plain_vector  plain_vector;

scalar_type distance (const base_node P, const base_node Q) {
    return sqrt((P[0]-Q[0])*(P[0]-Q[0]) + (P[1]-Q[1])*(P[1]-Q[1]));
}

void problemFracturePHFI::initialize(const std::vector < bgeot::base_node > & intersection, const bgeot::base_node tip, const std::vector < bgeot::base_node > & intersectionOut) {

    crackTip = tip;
    
	// On 106 we set the initial fracture
    FractureDirichletDofsC = (*mf_frac_).basic_dof_on_region(106);

    getfem::mesh_region border_faces;
    getfem::outer_faces_of_mesh(*mesh_p_, border_faces);
    
    scalar_type yA = intersection[0][1];
    scalar_type yB = intersection[2][1];
    assert(yB > yA);
    
    scalar_type yA_out = intersectionOut[0][1];
    scalar_type yB_out = intersectionOut[2][1];
    assert(yB_out > yA_out);
    
    (*mesh_p_).region(1010).clear();
    (*mesh_p_).region(1011).clear();
    
    for (getfem::mr_visitor i(border_faces); !i.finished(); ++i) {
        assert(i.is_face());
        
        bgeot::basic_mesh::ref_mesh_face_pt_ct pointsOfCurrentFace = (*mesh_p_).points_of_face_of_convex(i.cv(), i.f());
        
        scalar_type x = 0.;
        scalar_type y = 0.;
        
        for (size_type k = 0; k < pointsOfCurrentFace.size(); k++) {
            
            x += pointsOfCurrentFace[k][0];
            y += pointsOfCurrentFace[k][1];
            //std::cout << "k = " << k << "\t x = " << pointsOfCurrentFace[k][0] << "\t y = " << pointsOfCurrentFace[k][1] << std::endl;
        }

        x = x/pointsOfCurrentFace.size();
        y = y/pointsOfCurrentFace.size();
        
        //std::cout << "x = " << x << "\t y = " << y << std::endl;

        if (x < tip[0] && y < yB && y > yA) {
            //if (!((y > 4*yB/5) || (y < 4*yA/5))) {
                (*mesh_p_).region(1010).add(i.cv(), i.f());
                //std::cout << "x = " << x << "\t y = " << y << std::endl;
            //}
        }
        else if (!(x > tip[0] && y < yB_out && y > yA_out)){
            (*mesh_p_).region(1011).add(i.cv(), i.f());
            //std::cout << "x = " << x << "\t y = " << y << std::endl;
        }

    }
    
    // Finding the Dirichlet dofs where c = 1
    FractureDirichletDofsA = (*mf_frac_).basic_dof_on_region(1011);

    // Finding the dofs where c = c_analytic or grad(c).n = |grad(c_analytic)|*cos(pi/2 + alpha - theta)
    regionNearToIntersection = 1010;
    DofsNearToIntersection = (*mf_frac_).basic_dof_on_region(regionNearToIntersection);

}

void problemFracturePHFI::solve(std::vector<scalar_type> &fracture, const std::vector<scalar_type> &u_k_, const std::vector<scalar_type> &p_k_, const size_type hasPropagated, const crack_segment & intersectionSegment) {

    std::cout << "Starting assembling phase..." << std::endl;

    nb_dof_phaseField = (*mf_frac_).nb_dof();
    gmm::resize(fracture, nb_dof_phaseField); gmm::clear(fracture);

    // Model description.
    getfem::model model;
    
    // Main unknown of the problem.
    model.add_fem_variable("c", *mf_frac_);
    model.add_initialized_fem_data("u", *mf_disp_, u_k_);
    model.add_initialized_fem_data("p", *mf_pres_, p_k_);
    
    // Elasticity brick
    model.add_initialized_fixed_size_data("lambda", plain_vector(1, lambda_lame_));
    model.add_initialized_fixed_size_data("mu", plain_vector(1, mu_lame_));
    getfem::add_linear_generic_assembly_brick(model, *im_, "c*(lambda*Trace((Grad_u+Grad_u')/2)*Trace((Grad_u+Grad_u')/2)+2*mu*((Grad_u+Grad_u')/2):((Grad_u+Grad_u')/2))*Test_c", -1);
    
    // c*[...] term
    model.add_initialized_fixed_size_data("alpha", plain_vector(1, alpha_));
    model.add_initialized_fixed_size_data("Gc", plain_vector(1, G_resistance_));
    model.add_initialized_fixed_size_data("epsilon", plain_vector(1, epsilon_));
    // bbbb getfem::add_linear_generic_assembly_brick(model, *im_, "2*c*Test_c*Grad_p.u - 2*(alpha-1)*c*Test_c*p*Trace((Grad_u+Grad_u')/2) + Gc*c*Test_c/epsilon", -1);
    getfem::add_linear_generic_assembly_brick(model, *im_, "Gc*c*Test_c/epsilon", -1);

    // grad(c).grad(v) term
    getfem::add_linear_generic_assembly_brick(model, *im_, "Gc*epsilon*Grad_c.Grad_Test_c", -1, true, true);
    
    // Source term
    getfem::add_source_term_generic_assembly_brick(model, *im_, "Test_c*Gc/epsilon", -1);
    
    // Setting artificial Dirichlet boundary condition on previous crack
    if(hasPropagated <= 1) {
        std::cout << "Setting artificial DBC for initial crack..." << std::endl;
        
        getfem::add_Dirichlet_condition_with_simplification(model, "c", 106);
        
    }
#if 1
    model.add_initialized_fixed_size_data("gamma", plain_vector(1, 1.e6));
    getfem::add_linear_generic_assembly_brick(model, *im_, "gamma*c*Test_c",  1011, true, true);
    getfem::add_source_term_generic_assembly_brick(model, *im_, "gamma*Test_c/((lambda*Trace((Grad_u+Grad_u')/2)*Trace((Grad_u+Grad_u')/2)+2*mu*((Grad_u+Grad_u')/2):((Grad_u+Grad_u')/2))*epsilon/Gc + 1)", 1011);
#else
    // Dirichlet BC
    model.add_initialized_fixed_size_data("gamma", plain_vector(1, 1.e20));
    getfem::add_linear_generic_assembly_brick(model, *im_, "gamma*c*Test_c",  1011, true, true);
    getfem::add_source_term_generic_assembly_brick(model, *im_, "gamma*Test_c/((lambda*Trace((Grad_u+Grad_u')/2)*Trace((Grad_u+Grad_u')/2)+2*mu*((Grad_u+Grad_u')/2):((Grad_u+Grad_u')/2))*epsilon/Gc - 2*(alpha-1)*p*Trace((Grad_u+Grad_u')/2)*epsilon/Gc + 2*(epsilon/Gc)*Grad_p.u + 1)", 1011);
    //getfem::add_source_term_generic_assembly_brick(model, *im_, "gamma*Test_c", 1011);
#endif
    
    std::vector<scalar_type> boundaryDatumIntersection;
    gmm::resize(boundaryDatumIntersection, nb_dof_phaseField); gmm::clear(boundaryDatumIntersection);

#if DIRICHLET_ON_INTERSECTION
    // Applying Dirichlet boundary conditions on intersected boundary
    bgeot::size_type iB;
    DofsNearToIntersection = (*mf_frac_).basic_dof_on_region(regionNearToIntersection);
    for (iB << DofsNearToIntersection; iB != bgeot::size_type(-1); iB << DofsNearToIntersection) {

        bgeot::base_node boundaryNode = (*mf_frac_).point_of_basic_dof(iB);
        
        bgeot::base_node projection = intersectionSegment.getProjection(boundaryNode);
        
        scalar_type dist = distance(projection, boundaryNode);
        
        boundaryDatumIntersection[iB] = 1. - exp(-dist/(epsilon_));
    }
    model.add_initialized_fem_data("cBC", *mf_frac_, boundaryDatumIntersection);
    getfem::add_Dirichlet_condition_with_simplification(model, "c", 1010, "cBC");

#else
    
    DofsNearToIntersection = (*mf_frac_).basic_dof_on_region(regionNearToIntersection);
    for (dal::bv_visitor i(DofsNearToIntersection); !i.finished(); ++i) {
        
        base_node currentNode = (*mf_frac_).point_of_basic_dof(i);
        base_node currentProjection = intersectionSegment.getProjection(currentNode);
        
        //std::cout << "Current Node: (" << currentNode[0] << ", " << currentNode[1] << "), Projection: (" << currentProjection[0] << ", " << currentProjection[1] << ") " << std::endl;
        
        scalar_type dist = distance(currentNode, currentProjection);
        
        scalar_type delta_x = currentNode[0] - crackTip[0];
        scalar_type delta_y = currentNode[1] - crackTip[1];
        
        scalar_type alpha;
        
        if (delta_x==0)
            alpha = 4*atan(1.);
        else
            alpha = atan(delta_y/delta_x) + 4*atan(1.)*(delta_x<0);
        
        
        //std::cout << "Alpha = " << alpha << "\t Dist = " << dist << std::endl;
        
        if (dist == 0)
            boundaryDatumIntersection[i] = 0.;
        else
            boundaryDatumIntersection[i] = G_resistance_*exp(-dist/epsilon_)*((currentNode[0]-currentProjection[0])*cos(alpha) + (currentNode[1]-currentProjection[1])*sin(alpha))/dist;
        
        //std::cout << "neumannFunction = " << neumannFunction[i] << std::endl;
        
        //scalar_type alpha = atan((tip[1] - currentNode[1])/(tip[0] - currentNode[0]));
        //scalar_type theta = atan(segment.slope);
        //neumannFunction[i] = G_resistance_*exp(-dist/epsilon_)*cos(2*atan(1) + alpha - theta);
        //std::cout << "Theta = " << theta << "\t Alpha = " << alpha << "\t Dist = " << dist << std::endl;
        //std::cout << "Gradient modulus = " << exp(-dist/epsilon_)/epsilon_ << std::endl;
        //std::cout << "Gradient angle = " << 2*atan(1) + alpha - theta << std::endl;
        
    }
    
    model.add_initialized_fem_data("gradcBC", *mf_frac_, boundaryDatumIntersection);

    getfem::add_source_term_generic_assembly_brick(model, *im_, "gradcBC*Test_c", 1010);
    
#endif
    
    double time;
#ifdef GMM_USES_MPI
    time = MPI_Wtime();
#else
    time = gmm::uclock_sec();
#endif

    // Generic solve.
    std::cout << "Total number of variables : " << model.nb_dof() << std::endl;
    gmm::iteration iter(1E-9, 1, 40000);
    getfem::standard_solve(model, iter);
    
    // Solution extraction
    gmm::copy(model.real_variable("c"), fracture);

    double ftime;
#ifdef GMM_USES_MPI
    ftime = MPI_Wtime();
#else
    ftime = gmm::uclock_sec();
#endif
    
    if (getfem::MPI_IS_MASTER())
        std::cout << "Linear system assembled and solved in " << ftime - time << " seconds." << std::endl;

}

void problemFracturePHFI::setIntegrationMethod (const getfem::mesh_im &im){
    im_ = &im;
}

void problemFracturePHFI::setFiniteElementFracture (const getfem::mesh_fem &mf){
    mf_frac_ = &mf;
}

void problemFracturePHFI::setFiniteElementDisplacement (const getfem::mesh_fem &mf){
    mf_disp_ = &mf;
}

void problemFracturePHFI::setFiniteElementPressure (const getfem::mesh_fem &mf){
    mf_pres_ = &mf;
}

void problemFracturePHFI::setMesh (getfem::mesh &mesh){
    mesh_p_ = &mesh;
}
