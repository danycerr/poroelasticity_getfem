//
//  main.cpp
//
//
//  Created by Bianca Giovanardi on 22/06/16.
//
//

#include "getfem/getfem_export.h"   /* export functions (save solution in a file)  */
#include "getfem/getfem_regular_meshes.h"
#include "getfem/getfem_import.h"
#include "getfem/getfem_mesh_level_set.h"
#include "getfem/getfem_mesh_fem_global_function.h"
#include "getfem/getfem_mesh_fem_level_set.h"
#include "getfem/getfem_mesh_im_level_set.h"
#include "getfem/getfem_mesh_fem_product.h"
#include "getfem/getfem_model_solvers.h"
#include "getfem/getfem_derivatives.h"
#include "gmm/gmm.h"
#include "../../Library/crack_segment.h"
#include "../../Library/fracture.h"
#include "problemFracturePHFI.h"
#include "problemDisplacementPHFI.h"
#include "cluster.h"
#include "supportingFunctions.h"
#include <sstream>
#include <fstream>

/* some Getfem++ types that we will be using */
using bgeot::scalar_type;        /* = double */
using bgeot::size_type;          /* = unsigned long */
using bgeot::base_node;          /* geometrical nodes(derived from base_small_vector)*/
using bgeot::dim_type;

typedef getfem::model_real_plain_vector  plain_vector;

void printNode(const base_node &node) {
    std::cout << "(" << node[0] << ", " << node[1] << ")\t";
}

scalar_type norm2(std::vector<scalar_type> v) {
    
    scalar_type norm = 0.;
    for (int i = 0; i < v.size(); ++i) {
        norm += v[i]*v[i];
    }
    return gmm::sqrt(norm);
}

scalar_type error2(std::vector<scalar_type> u, std::vector<scalar_type> v) {
    scalar_type norm = 0.;
    for (int i = 0; i < v.size(); ++i) {
        norm += (u[i]-v[i])*(u[i]-v[i]);
    }
    return gmm::sqrt(norm);
}

int main(int argc, char *argv[]) {
    
    double initialTime;
#ifdef GMM_USES_MPI
    initialTime = MPI_Wtime();
#else
    initialTime = gmm::uclock_sec();
#endif
    
    // ---------------------------------------------------------------------------------------------
    // ------------------------ IMPORTING THE PARAMETERS -------------------------------------------
    // ---------------------------------------------------------------------------------------------
    
    // Read parameters from file .param
    bgeot::md_param PARAM;
    PARAM.read_command_line(argc, argv);
    
    // The physical parameters
    const scalar_type lambda = PARAM.real_value("LAMBDA", "1st Lame parameter [Pa]");
    const scalar_type mu = PARAM.real_value("MU", "2nd Lame parameter [Pa]");
    const scalar_type viscosity = PARAM.real_value("VISCOSITY", "Fluid viscosity");
    const scalar_type permeability = PARAM.real_value("PERMEABILITY", "Matrix permeability");
    const scalar_type inflow = PARAM.real_value("INFLOW_BC", "Inflow boundary condition");
    const scalar_type epsilon = PARAM.real_value("EPSILON", "Characteristic dimension of fracture [m]");
    const scalar_type resistance = PARAM.real_value("RESISTANCE", "Resistance of fracture [N/m]");
    const scalar_type alpha = PARAM.real_value("ALPHA", "Biot parameter [-]");
    
    // The numerical parameters
    const scalar_type eta_eps = 0.;
    std::string vtkPath = PARAM.string_value("VTK_PATH", "The path to save VTK results in");
    
    // The XFEM mesh
    const std::string MESH_FILE_XFEM = PARAM.string_value("MESH_FILE_XFEM", "Which mesh file to import for the Xfem.");
    const std::string MESH_TYPE = PARAM.string_value("MESH_TYPE","Mesh type ");
    bgeot::pgeometric_trans pgt = bgeot::geometric_trans_descriptor(MESH_TYPE);
    size_type N = pgt->dim();
    
    getfem::mesh meshXFEM;
    
    // Import the mesh if mesh file available...
    if (MESH_FILE_XFEM != "NOMESH")
        getfem::import_mesh(MESH_FILE_XFEM, meshXFEM);
    // ...or build a structured one
    else {
        std::vector<size_type> nsubdiv(N);
        nsubdiv[0] = PARAM.int_value("NX", "Number of space steps ");
        nsubdiv[1] = PARAM.int_value("NY", "Number of space steps ");
        getfem::regular_unit_mesh(meshXFEM, nsubdiv, pgt);
        bgeot::base_matrix M(N,N);
        for (size_type i=0; i < N; ++i) {
            static const char *t[] = {"LX","LY"};
            M(i,i) = (i<N) ? PARAM.real_value(t[i],t[i]) : 1.0;
        }
        meshXFEM.transformation(M);
        base_small_vector tt(N); tt[1] = -PARAM.real_value("LY")*0.5;
        meshXFEM.translation(tt);
    }
    
    {
        // Export the mesh in .vtk
        getfem::vtk_export vtkMesh(vtkPath + "MeshXFEM.vtk");
        vtkMesh.exporting(meshXFEM);
        vtkMesh.write_mesh();
    }
    
    const scalar_type X_max = PARAM.real_value("X_MAX");
    
    // ---------------------------------------------------------------------------------------------
    // ------------------------ SETTING THE FINITE ELEMENTS ----------------------------------------
    // ---------------------------------------------------------------------------------------------
    
    // The FEMs
    const std::string FEM_TYPE_DISPLACEMENT = PARAM.string_value("FEM_TYPE_DISPLACEMENT","Fem (displacement)");
    const std::string FEM_TYPE_PRESSURE = PARAM.string_value("FEM_TYPE_PRESSURE","Fem (pressure)");
    const std::string FEM_TYPE_PHASE_FIELD = PARAM.string_value("FEM_TYPE_PHASE_FIELD","Fem (phase field)");
    const std::string FEM_TYPE_GRAD_PHFI = PARAM.string_value("FEM_TYPE_GRAD_PHFI","Fem (gradient)");

    // The integration methods
    const std::string INTEGRATION = PARAM.string_value("INTEGRATION", "Integration method");
    const std::string SIMPLEX_INTEGRATION = PARAM.string_value("SIMPLEX_INTEGRATION", "Name of simplex integration method");
    const std::string SINGULAR_INTEGRATION = PARAM.string_value("SINGULAR_INTEGRATION");
    
    // Setting the finite elements for the pressure
    getfem::pfem pf_pressure = getfem::fem_descriptor(FEM_TYPE_PRESSURE);
    getfem::mesh_fem mf_pressure(meshXFEM);
    mf_pressure.set_finite_element(meshXFEM.convex_index(), pf_pressure);
    size_type pressure_dofs = mf_pressure.nb_dof();
    
    // ---------------------------------------------------------------------------------------------
    // ------------------------ SUMMARY OUTPUT -----------------------------------------------------
    // ---------------------------------------------------------------------------------------------
    
    // Summary output
    std::cout << std::endl;
    std::cout << "FINITE ELEMENT DISPLACEMENT  = " << FEM_TYPE_DISPLACEMENT << std::endl;
    std::cout << "FINITE ELEMENT PRESSURE      = " << FEM_TYPE_PRESSURE << std::endl;
    std::cout << "FINITE ELEMENT PHASE FIELD   = " << FEM_TYPE_PHASE_FIELD << std::endl;
    std::cout << "INTEGRATION                  = " << INTEGRATION << std::endl;
    
    size_type nb_cv = 0;
    dal::bit_vector nn = meshXFEM.convex_index();
    
    bgeot::size_type i_cv;
    for (i_cv << nn; i_cv != bgeot::size_type(-1); i_cv << nn)
        nb_cv++;
    
    std::cout << "Mesh for XFEM: " << std::endl;
    std::cout << "# ELEMENTS: " << nb_cv << std::endl;
    std::cout << "# VERTICES: " << (meshXFEM.points()).size() << std::endl;
    std::cout << std::endl;
    
    // ---------------------------------------------------------------------------------------------
    // ------------------------ INITIAL FRACTURE ----------------------------------------------
    // ---------------------------------------------------------------------------------------------
    
    scalar_type crackLengthPrev = 9;
    scalar_type crackLengthCurr = 9.51219;
    std::string fileOpeningPrev("length9.csv");
    std::string fileOpeningCurr("length9.51219.csv");

    base_node P(0, 0);
    base_node Q(crackLengthPrev, 0);
    fracture frac(P,Q);
    frac.print();
    
    // ---------------------------------------------------------------------------------------------
    // ------------------------ SETTING THE FEMs FOR XFEM ----------------------------------
    // ---------------------------------------------------------------------------------------------
    
    getfem::mesh_level_set              mls(meshXFEM);
    getfem::level_set                   ls(meshXFEM, 1, true);
    mls.add_level_set(ls);
    
    ls.reinit();
    std::cout << "ls.get_mesh_fem().nb_dof() = " << (ls.get_mesh_fem()).nb_dof() << "\n";
    for (size_type d = 0; d < ls.get_mesh_fem().nb_basic_dof(); ++d) {
        ls.values(0)[d] = frac.level_set_function(ls.get_mesh_fem().point_of_basic_dof(d))[0];
        ls.values(1)[d] = frac.level_set_function(ls.get_mesh_fem().point_of_basic_dof(d))[1];
    }
    ls.touch();
    mls.adapt();
    
    getfem::mesh mcut;
    mls.global_cut_mesh(mcut);
    
    // Enriched space for pressure
    getfem::mesh_fem_level_set  mfls_pressure(mls, mf_pressure);
    mfls_pressure.adapt();
    size_type enriched_pressure_dofs = mfls_pressure.nb_dof();
    
    // ---------------------------------------------------------------------------------------------
    // ------------------------ BUILDING MINIMESH --------------------------------------------------
    // ---------------------------------------------------------------------------------------------

    const std::string MESH_FILE_PHFI = PARAM.string_value("MESH_FILE_PHFI", "Which mesh file to import for the miniMesh.");
    
    getfem::mesh meshTip;
    getfem::import_mesh(MESH_FILE_PHFI, meshTip);
    
    scalar_type miniMeshDiameter = 10*epsilon;
    
    for (size_type i = 0; i < (meshTip.points()).size(); i++){
        (meshTip.points()[i])[0] = (meshTip.points()[i])[0]*miniMeshDiameter;
        (meshTip.points()[i])[1] = (meshTip.points()[i])[1]*miniMeshDiameter;
    }
    
    getfem::mesh miniMeshR(meshTip);
    for (size_type i = 0; i < (miniMeshR.points()).size(); i++){
        (miniMeshR.points()[i])[0] = (miniMeshR.points()[i])[0]+(frac.tips[1])[0];
        (miniMeshR.points()[i])[1] = (miniMeshR.points()[i])[1]+(frac.tips[1])[1];
        //std::cout << i << " (" << (miniMeshR.points()[i])[0] << ", " << (miniMeshR.points()[i])[1] << ") " << std::endl;
    }
    
    scalar_type scaleFactor = 1.5;
    miniMeshDiameter = miniMeshDiameter*scaleFactor;
    std::cout << "Mini-mesh: " << std::endl;
    std::cout << "\t Diameter: " << miniMeshDiameter << std::endl;
    scalar_type maxRadius = miniMeshR./*maximal*/minimal_convex_radius_estimate();
    std::cout << "\t Maximum radius: " << maxRadius << std::endl;
    
    {
        // Export the mesh in .vtk
        getfem::vtk_export vtkMesh(vtkPath + "miniMeshR.vtk");
        vtkMesh.exporting(miniMeshR);
        vtkMesh.write_mesh();
    }
    
    // ---------------------------------------------------------------------------------------------
    // ------------------------ BUILDING THE FINITE ELEMENTS ON MINIMESH ---------------------------
    // ---------------------------------------------------------------------------------------------
    
    // Setting the framework for the coupled problem on miniMesh
    
    // Setting the integration method
    getfem::pintegration_method ppi = getfem::int_method_descriptor(INTEGRATION);
    getfem::mesh_im im(miniMeshR);
    im.set_integration_method(miniMeshR.convex_index(), ppi);
    
    // Setting the finite elements for the displacement...
    getfem::mesh_fem mf_displacement_PHFI(miniMeshR);
    mf_displacement_PHFI.set_qdim(N);
    getfem::pfem pf_displacement = getfem::fem_descriptor(FEM_TYPE_DISPLACEMENT);
    mf_displacement_PHFI.set_finite_element(miniMeshR.convex_index(), pf_displacement);
    size_type displacement_dofs_PHFI = mf_displacement_PHFI.nb_dof();
    
    // Setting the finite elements for the pressure...
    getfem::mesh_fem mf_pressure_PHFI(miniMeshR);
    mf_pressure_PHFI.set_finite_element(miniMeshR.convex_index(), pf_pressure);
    size_type pressure_dofs_PHFI = mf_pressure_PHFI.nb_dof();

    // ... and for the phase field variable
    getfem::pfem pf_phaseField = getfem::fem_descriptor(FEM_TYPE_PHASE_FIELD);
    getfem::mesh_fem mf_phaseField(miniMeshR);
    mf_phaseField.set_finite_element(miniMeshR.convex_index(), pf_phaseField);
    size_type phaseField_dofs = mf_phaseField.nb_dof();
    
    // ... and for the old phase field
    getfem::mesh miniMeshR_old(miniMeshR);
    getfem::mesh_fem mf_phaseField_old(miniMeshR_old);
    mf_phaseField_old.set_finite_element(miniMeshR_old.convex_index(), pf_phaseField);
    
    // ... and for the gradient of c
    getfem::mesh_fem mf_gradC(miniMeshR);
    getfem::pfem pf_gradC = getfem::fem_descriptor(FEM_TYPE_GRAD_PHFI);
    mf_gradC.set_finite_element(miniMeshR.convex_index(), pf_gradC);
    
    std::vector<scalar_type> gradC;
    gmm::resize(gradC, N*(mf_gradC).nb_dof()); gmm::clear(gradC);
    
    // ---------------------------------------------------------------------------------------------
    // ---------------------------- PROPAGATION PROBLEM DEFINITION ---------------------------------
    // ---------------------------------------------------------------------------------------------

    problemFracturePHFI         problemFractureMiniMesh(eta_eps, lambda, mu, epsilon, alpha, resistance);
    problemFractureMiniMesh.setMesh(miniMeshR);
    problemFractureMiniMesh.setIntegrationMethod(im);
    problemFractureMiniMesh.setFiniteElementDisplacement(mf_displacement_PHFI);
    problemFractureMiniMesh.setFiniteElementFracture(mf_phaseField);
    problemFractureMiniMesh.setFiniteElementPressure(mf_pressure_PHFI);
    
    problemDisplacementPHFI     problemDisplacementMiniMesh(eta_eps, lambda, mu, epsilon, alpha, resistance);
    problemDisplacementMiniMesh.setMesh(miniMeshR);
    problemDisplacementMiniMesh.setIntegrationMethod(im);
    problemDisplacementMiniMesh.setFiniteElementDisplacement(mf_displacement_PHFI);
    problemDisplacementMiniMesh.setFiniteElementFracture(mf_phaseField);
    problemDisplacementMiniMesh.setFiniteElementPressure(mf_pressure_PHFI);
    
    std::ifstream pressureVect("values_pressure");
    std::ifstream displacementVect("values_displ");
    
    std::vector<scalar_type> u_BC;
    gmm::resize(u_BC, mf_displacement_PHFI.nb_dof()); gmm::clear(u_BC);
    
    std::string s;
    size_type i=0;
    while (getline(displacementVect, s))
    {
        u_BC[i] = atof(s.c_str());
        i++;
    }

    std::vector<scalar_type> p_new;
    gmm::resize(p_new, enriched_pressure_dofs); gmm::clear(p_new);

    i=0;
    while (getline(pressureVect, s))
    {
        p_new[i] = (atof(s.c_str()));
        i++;
    }

    {
        std::string vtkFilename;
        
        // Export the pressure in .vtk
        vtkFilename = vtkPath + "DisplacementPHFI.vtk";
        std::cout << "Exporting displacement in " << vtkFilename << std::endl;
        getfem::vtk_export vtkDisp(vtkFilename);
        vtkDisp.exporting(mf_displacement_PHFI);
        vtkDisp.write_mesh();
        vtkDisp.write_point_data(mf_displacement_PHFI, u_BC, "DisplacementPHFI");
        
        // Export the pressure in .vtk
        vtkFilename = vtkPath + "Pressure.vtk";
        std::cout << "Exporting pressure in " << vtkFilename << std::endl;
        getfem::vtk_export vtkPres(vtkFilename);
        vtkPres.exporting(mfls_pressure);
        vtkPres.write_mesh();
        vtkPres.write_point_data(mfls_pressure, p_new, "Pressure");

    }

    // ---------------------------------------------------------------------------------------------
    // ------------------------ FINDING INTERSECTIONS ----------------------------------------------
    // ---------------------------------------------------------------------------------------------

    // Intersections
    crack_segment intersectionSegment = frac.segments.back();
    std::vector < bgeot::base_node > intersection(3);
    std::vector < bgeot::base_node > intersectionOut(3);
    
    scalar_type xtip = frac.tips[1][0];
    scalar_type ytip = frac.tips[1][1];
    scalar_type radius = 0.5*miniMeshDiameter;
    
    scalar_type widthIntersection = 1.4*epsilon;
    
    // A
    scalar_type yA = -widthIntersection;
    intersection[0] = bgeot::base_node(xtip - sqrt(radius*radius - (yA-ytip)*(yA-ytip)), yA);
    scalar_type yA_out = -0.5*sqrt(2.)*radius;
    intersectionOut[0] = bgeot::base_node(xtip + sqrt(radius*radius - (yA_out-ytip)*(yA_out-ytip)), yA_out);
    
    // C
    intersection[1] = bgeot::base_node(xtip - radius, 0.);
    intersectionOut[1] = bgeot::base_node(xtip + radius, 0.);
    
    // B
    scalar_type yB = widthIntersection;
    intersection[2] = bgeot::base_node(xtip - sqrt(radius*radius - (yB-ytip)*(yB-ytip)), yB);
    scalar_type yB_out = 0.5*sqrt(2.)*radius;
    intersectionOut[2] = bgeot::base_node(xtip + sqrt(radius*radius - (yB_out-ytip)*(yB_out-ytip)), yB_out);
    
    //////////////////////
    std::cout << "IN:" << std::endl;
    std::cout << "A : (" << intersection[0][0] << ", " << intersection[0][1] << ") " << std::endl;
    std::cout << "B : (" << intersection[2][0] << ", " << intersection[2][1] << ") " << std::endl;
    std::cout << "C : (" << intersection[1][0] << ", " << intersection[1][1] << ") " << std::endl;
    
    std::cout << "OUT:" << std::endl;
    std::cout << "A : (" << intersectionOut[0][0] << ", " << intersectionOut[0][1] << ") " << std::endl;
    std::cout << "B : (" << intersectionOut[2][0] << ", " << intersectionOut[2][1] << ") " << std::endl;
    std::cout << "C : (" << intersectionOut[1][0] << ", " << intersectionOut[1][1] << ") " << std::endl;
    
    // ---------------------------------------------------------------------------------------------
    // ------------------------ PROPAGATION WITH PHASE FIELD ---------------------------------------
    // ---------------------------------------------------------------------------------------------

    std::cout << "------------------------------------------------------------------------" << std::endl;
    std::cout << "          Computing propagation with PHFI:           				              " << std::endl;
    std::cout << "------------------------------------------------------------------------" << std::endl;

    scalar_type error = 1;
    size_type kk = 0;
    size_type Kmax = 100;
    
    std::vector<scalar_type> u_new_PHFI;
    gmm::resize(u_new_PHFI, displacement_dofs_PHFI); gmm::clear(u_new_PHFI);
    for (size_type i=0; i < displacement_dofs_PHFI; ++i) {
        u_new_PHFI[i] = u_BC[i];
    }
    
    getfem::mesh_fem mf_exp_pressure(mcut);
    mf_exp_pressure.set_classical_discontinuous_finite_element(1, 0.001);
    plain_vector p_new_exp(mf_exp_pressure.nb_dof());
    getfem::interpolation(mfls_pressure, mf_exp_pressure, p_new, p_new_exp);

    /*
    {
        // Export the pressure in .vtk
        std::string vtkFilename = vtkPath + "Pressure.Interp.vtk";
        std::cout << "Exporting pressure in " << vtkFilename << std::endl;
        getfem::vtk_export vtkPres(vtkFilename);
        vtkPres.exporting(mf_exp_pressure);
        vtkPres.write_mesh();
        vtkPres.write_point_data(mf_exp_pressure, p_new_exp, "Pressure");

    }*/
    
    std::vector<scalar_type> p_old_PHFI;
    gmm::resize(p_old_PHFI, pressure_dofs_PHFI); gmm::clear(p_old_PHFI);
    getfem::interpolation(mf_exp_pressure, mf_pressure_PHFI, p_new_exp, p_old_PHFI);

    std::vector<scalar_type> c_new;
    gmm::resize(c_new, phaseField_dofs); gmm::clear(c_new);

    std::vector<scalar_type> c_old;
    gmm::resize(c_old, phaseField_dofs); gmm::clear(c_old);

    bool aa = 1;

    problemFractureMiniMesh.initialize(intersection, frac.tips[1], intersectionOut);

    while (aa) {
        
        std::cout << "\nIteration number:\t" << kk << std::endl;
        
        std::cout << "------------------------------------------------------------------------" << std::endl;
        std::cout << "          Fracture :                                                    " << std::endl;
        std::cout << "------------------------------------------------------------------------" << std::endl;
        
        problemFractureMiniMesh.solve(c_new, u_new_PHFI, p_old_PHFI, 0, intersectionSegment);
        
        std::cout << "------------------------------------------------------------------------" << std::endl;
        std::cout << "          Displacement :                                                " << std::endl;
        std::cout << "------------------------------------------------------------------------" << std::endl;
        
        gmm::resize(u_new_PHFI, displacement_dofs_PHFI); gmm::clear(u_new_PHFI);
        problemDisplacementMiniMesh.solve(u_new_PHFI, c_new, u_BC, p_old_PHFI);

        std::vector<scalar_type> diffC(phaseField_dofs);
        for (size_type i = 0; i < phaseField_dofs; ++i) {
            diffC[i] = c_new[i] - c_old[i];
        }
        scalar_type error = getfem::asm_H1_norm (im, mf_phaseField, diffC)/getfem::asm_H1_norm (im, mf_phaseField, c_old);

        std::cout << "\tTotal increment = " << error << std::endl;

        {
            std::string vtkFilename;
            std::ostringstream number;
            number << kk;

            // Export the pressure in .vtk
            vtkFilename = vtkPath + "Fracture." + number.str() + ".vtk";
            std::cout << "Exporting fracture (phase field) in " << vtkFilename << std::endl;
            getfem::vtk_export vtkFrac(vtkFilename);
            vtkFrac.exporting(mf_phaseField);
            vtkFrac.write_mesh();
            vtkFrac.write_point_data(mf_phaseField, c_new, "Fracture");
            
            // Export the displacement in .vtk
            vtkFilename = vtkPath + "DisplacementMiniMesh." + number.str() + ".vtk";
            std::cout << "Exporting displacement (phase field) in " << vtkFilename << std::endl;
            getfem::vtk_export vtkDisp(vtkFilename);
            vtkDisp.exporting(mf_displacement_PHFI);
            vtkDisp.write_mesh();
            vtkDisp.write_point_data(mf_displacement_PHFI, u_new_PHFI, "Displacement");

            // Export the pressure in .vtk
            vtkFilename = vtkPath + "PressureMiniMesh." + number.str() + ".vtk";
            std::cout << "Exporting pressure (phase field) in " << vtkFilename << std::endl;
            getfem::vtk_export vtkPres(vtkFilename);
            vtkPres.exporting(mf_pressure_PHFI);
            vtkPres.write_mesh();
            vtkPres.write_point_data(mf_pressure_PHFI, p_old_PHFI, "Pressure");

        }
        
        kk++;
        
        for (size_type i = 0; i < phaseField_dofs; ++i) {
            c_old[i] = c_new[i];
        }

        aa = ((kk < Kmax) && (error > 1.e-2));
    }
    
    // ---------------------------------------------------------------------------------------------
    // ------------------------ COMPUTE NEW TIP ----------------------------------------------------
    // ---------------------------------------------------------------------------------------------
    
    std::cout << "Computing crack tip..." << std::endl;
    
#if 1
    getfem::compute_gradient(mf_phaseField, mf_gradC, c_new, gradC);
    
    std::vector<scalar_type> gradCnorm;
    gmm::resize(gradCnorm, (mf_gradC).nb_dof()); gmm::clear(gradCnorm);
    
    std::vector<scalar_type> gradCx;
    gmm::resize(gradCx, (mf_gradC).nb_dof()); gmm::clear(gradCx);
    
    // Compute the norm of the gradient for each element
    for (size_type i = 0; i < mf_gradC.nb_dof(); i++){
        gradCx[i] = gradC[2*i];
        scalar_type gradCy = gradC[2*i+1];
        gradCnorm[i] = sqrt(gradCx[i]*gradCx[i] + gradCy*gradCy);
    }
    scalar_type gradCxMax = *max_element(gradCx.begin(), gradCx.end());
    
    std::cout << "max grad x" << gradCxMax << std::endl;
    
    {
        // Export the pressure in .vtk
        std::string vtkFilename = vtkPath + "Gradient.vtk";
        std::cout << "Exporting gradient (phase field) in " << vtkFilename << std::endl;
        getfem::vtk_export vtkPres(vtkFilename);
        vtkPres.exporting(mf_gradC);
        vtkPres.write_mesh();
        vtkPres.write_point_data(mf_gradC, gradCx, "GradientX");
    }
    
    std::vector< std::vector< bgeot::base_node > >  cluster;
    
    // Organize in the cluster of the tips the elements whose norme of gradient is higher (in the sense that > sth*maxGradient)
    
    bgeot::base_node        tip_prev = frac.tips[1];
    
    for (size_type i = 0; i < mf_gradC.nb_dof(); i++){
        if(gradCx[i] > 0.75*gradCxMax) {
            bgeot::base_node node = mf_gradC.point_of_basic_dof(i);
            if ((node[0]-tip_prev[0])*(node[0]-tip_prev[0]) + (node[1]-tip_prev[1])*(node[1]-tip_prev[1]) < 0.8*radius*0.8*radius) {
                scalar_type radius = 3*miniMeshR.convex_radius_estimate(mf_gradC.first_convex_of_basic_dof(i));
                organize_in_cluster(node, cluster, radius);
            }
        }
    }
    
    size_type mostPopularI = 0;
    for (size_type i = 0; i < cluster.size(); i++){
        if(cluster[i].size()>cluster[mostPopularI].size())
            mostPopularI = i;
    }
    std::vector< bgeot::base_node > mostPopularCluster = cluster[mostPopularI];
    
    bgeot::base_node        tip_curr;
    // Compute the tips by averaging the nodes of each cluster
    //std::cout << "Tip: " << std::endl;
    scalar_type x = 0;
    scalar_type y = 0;
    for (size_type j = 0; j < mostPopularCluster.size(); ++j) {
        x += (mostPopularCluster[j])[0];
        // y += (mostPopularCluster[j])[1]; BBBB Artificially setting y = 0
        //std::cout << "(" << ((cluster[i])[j])[0] << ", " << ((cluster[i])[j])[1] << ")" << std::endl;
    }
    x = x/(mostPopularCluster).size();
    // y = y/(mostPopularCluster).size(); BBBB Artificially setting y = 0
#else
    bgeot::base_node tip_curr;
    scalar_type x = 5.7;
    scalar_type y = 0.;
#endif

    tip_curr = bgeot::base_node(x,y);
    std::cout << "New tip is ";
    printNode(tip_curr);
    std::cout << std::endl;
    
    return 0;
}
