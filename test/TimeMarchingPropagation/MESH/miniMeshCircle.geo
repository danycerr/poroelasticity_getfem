// The unit is 10*epsilon, so that epsilon/h=0.1*h^-1
// This minimesh is rescaled with the epsilon of
// the simulation in the main.
// The variable scale indicates how large is the minimesh.
// The same value scale is to be set in the main.
h = 0.005;
H = 0.02;
scale  = 1.5;

Point(1) = {-0.5*scale, 0,  0, h} ;
Point(2) = {0, -0.5*scale,  0, H} ;
Point(3) = {0.5*scale, 0,  0, H} ;
Point(4) = {0, 0.5*scale,  0, H} ;

Point(5) = {0, 0,  0, h} ;

Circle(1) = {1,5,2} ;
Circle(2) = {2,5,3} ;
Circle(3) = {3,5,4} ;
Circle(4) = {4,5,1} ;

Line Loop(10) = {1,2,3,4} ;

Line(5) = {1,5} ;

Plane Surface(13) = {10} ;

Line{5} In Surface{13};

Physical Line(106) = {5} ;
Physical Line(107) = {1,2,3,4} ;

Physical Surface(14) = {13};
