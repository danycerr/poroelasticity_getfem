//
//  cluster.h
//  
//
//  Created by Bianca Giovanardi on 22/05/16.
//
//

#ifndef ____cluster__
#define ____cluster__

#include<tuple>
#include "getfem/getfem_assembling.h"

using bgeot::scalar_type;
using bgeot::base_node;

class cluster {
    
private:

    std::vector < std::tuple < bgeot::base_node, scalar_type, scalar_type > > setOfElements;
    bgeot::base_node center;
    scalar_type averagePhaseFieldOnSegment;
    
public:

    cluster(const std::tuple < bgeot::base_node, scalar_type, scalar_type > element);
    cluster() = default;
    void print() const;
    void computeCenter();
    void addElement (const std::tuple < bgeot::base_node, scalar_type, scalar_type > elementToAdd);
    bgeot::base_node getCenter() const;
    void setAverageOfC(const scalar_type average);
    bool clusterShouldContainTuple (const std::tuple < bgeot::base_node, scalar_type, scalar_type > & tupleToTest, const scalar_type radius) const;
    bool shouldBeMergedWithCluster(const cluster & myCluster, const scalar_type radius) const;
};

#endif /* defined(____cluster__) */
