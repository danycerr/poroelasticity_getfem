//
//  problemFracturePHFI.h
//  
//
//  Created by Bianca Giovanardi on 01/10/14.
//
//

#ifndef ____problemFracturePHFI__
#define ____problemFracturePHFI__

#include "getfem/getfem_export.h" 
#include "../../Library/crack_segment.h"

/* Some Getfem++ types that we will be using: */
using bgeot::scalar_type;        /* = double */
using bgeot::size_type;          /* = unsigned long */

/* Definition of some matrix/vector types: */
typedef gmm::rsvector<scalar_type> sparse_vector_type;
typedef gmm::row_matrix<sparse_vector_type> sparse_matrix_type;

/**
 * \class problemFractureEvolution
 * This class solves one Newton iteration, given the solution of the previous Newton iteration and that of the previous time iteration, of the following system:
 *
 *    \f$ \nabla \cdot \left( (\eta_{\epsilon} + c^2) (\lambda \text{tr}\mathbf{\varepsilon}(\mathbf{u}) \mathbf{I} + 2\mu \mathbf{\varepsilon}(\mathbf{u}) ) \right)  = \mathbf{0} \f$
 *
 *    \f$ \frac{1}{M} \dot{c} + c \mathbf{\varepsilon} \cdot \mathbf{\sigma} - \epsilon \Delta c - \frac{1}{\epsilon} (1 - c) = 0 \f$
 *
 *   TO BE COMPLETED WITH BOUNDARY CONDITIONS
 */

class problemFracturePHFI {

private:

    // Data (vectors for the finite element functions and scalar for the constants)
    scalar_type epsilon_;                           // Characteristic dimension of the fracture
    scalar_type G_resistance_;                                 // Extension of the fracture along the x axis
    scalar_type eta_eps_;                           // Coefficient for residual stress
    scalar_type lambda_lame_;                       // 1st Lame' parameter
    scalar_type mu_lame_;                           // 2nd Lame' parameter
    scalar_type alpha_;                             // Biot paramenter

    // Pointers to the integration methods
    const getfem::mesh_im *im_ = NULL;      // Integration method for fracture
    
    // Pointers to the finite elements methods
    const getfem::mesh_fem *mf_frac_ = NULL;     // Finite elements method for fracture
    const getfem::mesh_fem *mf_disp_ = NULL;     // Finite elements method for fracture
    const getfem::mesh_fem *mf_pres_ = NULL;     // Finite elements method for fracture

    bgeot::base_node crackTip;
    
    // Vectors containing the region flags of Dirichlet boundary and Neumann boundary.
    dal::bit_vector FractureDirichletDofsA;
    size_type regionNearToIntersection;
    dal::bit_vector DofsNearToIntersection;
    dal::bit_vector FractureDirichletDofsC;

    size_type nb_dof_phaseField;
        
protected:
    // The mesh, its type, and the dimension of the domain
    //! @cond
    getfem::mesh *mesh_p_ = NULL;                // Pointer to the mesh
    //! @endcond
    
public:
    //! @name Constructor and Destructor
    //@{
    
    //! \brief The constructor.
    /**
     *  @param fracture         The variable that will contain the fracture solution of the system.
     *  @param eta_eps          Coefficient for residual stress
     *  @param lambda_lame      1st Lame' parameter
     *  @param mu_lame          2nd Lame' parameter
     *  @param epsilon          Characteristic dimension of the fracture
     *  @param mobility         Mobility
     *  @param b                Extension of the fracture along the y axis (starting from x=0,y=0)
     *  @param sigma_top_Y
     *  @param u_n              Previous time-step displacement solution [mf_displacement]
     *  @param c_n              Previous time-step phase field solution [mf_phaseField]
     *  @param u_k              Previous alternate iteration displacement solution [mf_displacement]
     */
    
    problemFracturePHFI(const scalar_type &eta_eps,
                        const scalar_type &lambda_lame,
                        const scalar_type &mu_lame,
                        const scalar_type &epsilon,
                        const scalar_type &alpha,
                        const scalar_type &G_resistance):
    eta_eps_(eta_eps), lambda_lame_(lambda_lame),
    mu_lame_(mu_lame), epsilon_(epsilon), alpha_(alpha), G_resistance_(G_resistance) {};
    
    //! \brief The default destructor.
    ~problemFracturePHFI() = default;
    
    
    //! @name Public setters
    //@{
    //! \brief Sets the integration method for fracture.
    void setIntegrationMethod (const getfem::mesh_im &im);
    //! \brief Sets the finite element space for fracture.
    void setFiniteElementFracture (const getfem::mesh_fem &mf);
    //! \brief Sets the finite element space for fracture.
    void setFiniteElementDisplacement (const getfem::mesh_fem &mf);
    //! \brief Sets the finite element space for fracture.
    void setFiniteElementPressure (const getfem::mesh_fem &mf);
    //! \brief Sets the type of the mesh (w.r.t. the getfem classification of geometric transformations) and the mesh itself.
    void setMesh (getfem::mesh &mesh);
    //@}
    
    //! \brief This function initializes the boundary conditions associated to the system.
    void initialize(const std::vector < bgeot::base_node > & intersection, const bgeot::base_node tip, const std::vector < bgeot::base_node > & intersectionOut);
    
    //! \brief This function solves the algebraic system built with the function assembly.
    void solve(std::vector<scalar_type> &fracture, const std::vector<scalar_type> &u_k_, const std::vector<scalar_type> &p_k_, const size_type hasPropagated, const crack_segment & intersectionSegment);
    
};

#endif /* defined(____problemFracturePHFI__) */
