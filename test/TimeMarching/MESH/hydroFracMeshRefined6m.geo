L = 60;
H = L*0.01;
h = 0.1*H;
len = 6;

Point(1) = {0, -0.5*L,  0, H} ;
Point(2) = {0, 0,  0, H} ;
Point(3) = {0, 0.5*L,  0, H} ;
Point(4) = {L, 0.5*L,  0, H} ;
Point(5) = {L, 0,  0, H} ;
Point(6) = {L, -0.5*L,  0, H} ;
Point(7) = {len+h, h, 0, h} ;

Line(1) = {1,2} ;
Line(2) = {2,3} ;
Line(3) = {3,4} ;
Line(4) = {4,5} ;
Line(5) = {5,6} ;
Line(6) = {6,1} ;

Line Loop(10) = {1,2,3,4,5,6} ;

// We can then define the surface as a list of line loops (only one
// here, since there are no holes--see `t4.geo'):

Plane Surface(13) = {10} ;

Point{7} In Surface{13};

Physical Line(101) = {1,2} ;
Physical Line(102) = {3} ;
Physical Line(103) = {4,5} ;
Physical Line(104) = {6} ;

Physical Surface(14) = {13};